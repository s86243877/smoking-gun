#[cfg(feature = "chacha")]
pub mod chacha;

#[cfg(feature = "poly1305")]
pub mod poly1305;

#[cfg(all(feature = "chacha", feature = "poly1305"))]
pub mod chacha20poly1305;
