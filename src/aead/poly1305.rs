//! Poly1305 implementation for Bulletproofs arithmetic circuit use.

use crate::utils::*;
use bulletproofs::r1cs::*;
use curve25519_dalek::scalar::Scalar;
use failure::*;

// Tupels are: (lower 65 bits, higher 65 bits)
pub struct Poly1305Gadget {
    r: (LinearCombination, LinearCombination),
    // lower 65 bits, higher 65 bits
    r_witness: Option<(Scalar, Scalar)>,

    // This doesn't need to be split in limbs
    s: LinearCombination,

    accumulator: (LinearCombination, LinearCombination),
    accumulator_witness: Option<(Scalar, Scalar)>,
}

// returns (upper, lower) 65 bits of a 16 byte slice, for use in scalars.
fn bytes_to_limbs(msg: &[u8]) -> ([u8; 32], [u8; 32]) {
    // lower should contain bits 0..65
    let mut lower = [0u8; 32];
    lower[..8].copy_from_slice(&msg[..8]);
    lower[8] = msg[8] & 1;

    // upper should contain bits 65..128.
    let mut upper = [0u8; 32];
    for i in 0..(msg.len() - 8) {
        upper[i] = msg[8 + i] >> 1;
        if msg.len() > 9 + i {
            upper[i] |= (msg[9 + i] & 1) << 7
        }
    }

    (lower, upper)
}

impl Poly1305Gadget {
    /// Construct a Poly1305 gadget starting from 256 bits of key material.
    ///
    /// Warning: Poly1305 is a *one-time* authenticator, a key should *not* be reused.
    pub fn new(key: &[LinearCombination], key_witness: Option<&[u8; 32]>) -> Result<Self, Error> {
        ensure!(key.len() == 256, "Need 256 key input wires");
        let accumulator = (LinearCombination::default(), LinearCombination::default());
        let mut r0 = LinearCombination::default();
        let mut r1 = LinearCombination::default();
        let mut s = LinearCombination::default();

        let r_witness = key_witness.map(|key| {
            // XXX incorrect!!!! should still shift one bit!
            let mut lower = [0u8; 32];
            let mut upper = [0u8; 32];
            lower[0..8].copy_from_slice(&key[0..8]);
            upper[8..16].copy_from_slice(&key[0..8]);

            lower[3] &= 15;
            lower[7] &= 15;

            upper[11 - 8] &= 15;
            upper[15 - 8] &= 15;
            lower[4] &= 252;

            upper[8 - 8] &= 252;
            upper[12 - 8] &= 252;
            (Scalar::from_bits(lower), Scalar::from_bits(upper))
        });
        // TODO: remove
        let _s_witness = key_witness.map(|key| {
            let mut w = [0u8; 32];
            w[0..16].copy_from_slice(&key[16..32]);
            Scalar::from_bits(w)
        });

        // The first 16 bytes make up `r`.
        //
        // From RFC7539:
        //  o  r[3], r[7], r[11], and r[15] are required to have their top four
        //     bits clear (be smaller than 16)
        //
        //  o  r[4], r[8], and r[12] are required to have their bottom two bits
        //     clear (be divisible by 4)
        for bit in 0..128 {
            let byte = bit / 8;
            if [3, 7, 11, 15].contains(&byte) && (bit % 8) > 3 {
                // These bits don't count
                continue;
            }
            if [4, 8, 12].contains(&byte) && (bit % 8) < 2 {
                // These bits don't count either
                continue;
            }

            // XXX: incorrect!!!! should still shift one bit
            if bit < 64 {
                r0 = r0 + Scalar::from(1u128 << bit) * key[bit].clone();
            } else {
                r1 = r1 + Scalar::from(1u128 << (bit - 64)) * key[bit].clone();
            }
        }

        // The second 16 bytes make up `s`.
        for bit in 0..128 {
            s = s + Scalar::from(1u128 << bit) * key[bit + 128].clone();
        }

        Ok(Poly1305Gadget {
            r: (r0, r1),
            r_witness,
            s,
            accumulator,
            accumulator_witness: if key_witness.is_some() {
                Some((Scalar::zero(), Scalar::zero()))
            } else {
                None
            },
        })
    }

    /// Absorb a full block of [u8].
    /// Requires 16 bytes as message; feed the last (possibly partial) through [`finalize`].
    pub fn absorb_block<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
        msg: &[u8],
    ) -> Result<(), Error> {
        ensure!(msg.len() == 16, "Message should be 16 bytes");
        self.absorb_partial(cs, msg)?;

        Ok(())
    }

    fn reduce_mod_p<CS: ConstraintSystem + ?Sized>(&mut self, cs: &mut CS) -> Result<(), Error> {
        Ok(())
    }

    fn mul_r_mod_p<CS: ConstraintSystem + ?Sized>(&mut self, cs: &mut CS) -> Result<(), Error> {
        // t0 = r0 * a0;
        // t1 = r0 * a1 + r1 * a0;
        let (_t0l, _t0r, t0) = cs.multiply(self.r.0.clone(), self.accumulator.0.clone());
        let t1 = {
            let (_l1, _r1, m1) = cs.multiply(self.r.0.clone(), self.accumulator.1.clone());
            let (_l2, _r2, m2) = cs.multiply(self.r.1.clone(), self.accumulator.0.clone());
            m1 + m2
        };
        // same for the witnesses
        let t_witness = self.accumulator_witness.as_ref().map(|(a0, a1)| {
            let (r0, r1) = self.r_witness.as_ref().unwrap();
            let t0 = r0 * a0;
            let t1 = r0 * a1 + r1 * a0;
            (t0, t1)
        });
        let t_witness_bytes = t_witness
            .as_ref()
            .map(|(t0, t1)| (t0.as_bytes(), t1.as_bytes()));

        // we need to dismantle t1 into its 130 bits for that,
        let mut t0_bits = Vec::with_capacity(130);
        let mut t1_bits = Vec::with_capacity(130);
        for i in 0..130 {
            t0_bits.push(allocate_bit(
                cs,
                t_witness_bytes
                    .as_ref()
                    .map(|(t0_witness, _)| (t0_witness[i / 8] & (1 << (i % 8))) != 0),
            )?);
            t1_bits.push(allocate_bit(
                cs,
                t_witness_bytes
                    .as_ref()
                    .map(|(_, t1_witness)| (t1_witness[i / 8] & (1 << (i % 8))) != 0),
            )?);
        }

        // mask carry
        let t0: LinearCombination = t0_bits
            .drain(0..65)
            .enumerate()
            .map(|(i, bit)| (bit, Scalar::from(1u128 << i)))
            .collect();

        let t1: LinearCombination = t1_bits
            .drain(0..65)
            .enumerate()
            .map(|(i, bit)| (bit, Scalar::from(1u128 << i)))
            .collect::<LinearCombination>()
            + t0_bits // carry bits from t0
                .drain(0..65)
                .enumerate()
                .map(|(i, bit)| (bit, Scalar::from(1u128 << i)))
                .collect::<LinearCombination>();

        // propagate high limb: t0 += (t1 >> 65) * 5;
        let t1_high: LinearCombination = t1_bits
            .drain(0..65)
            .enumerate()
            .map(|(i, bit)| (bit, Scalar::from(1u128 << i)))
            .collect();

        let t0 = t0 + t1_high * Scalar::from(5u32);
        self.accumulator = (t0, t1);

        // drag along the witness
        self.accumulator_witness = t_witness_bytes.map(|(t0_bytes, t1_bytes)| {
            let (t0, t0_overflow) = bytes_to_limbs(t0_bytes);
            let (t1, t1_overflow) = bytes_to_limbs(t1_bytes);
            let t0 = Scalar::from_bits(t0);
            let t0_overflow = Scalar::from_bits(t0_overflow);
            let t1 = Scalar::from_bits(t1);
            let t1_overflow = Scalar::from_bits(t1_overflow);
            (t0 + Scalar::from(5u32) * t1_overflow, t0_overflow + t1)
        });
        Ok(())
    }

    pub(crate) fn absorb_partial<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
        msg: &[u8],
    ) -> Result<(), Error> {
        let len = msg.len();
        ensure!(len <= 16, "Message should be at most 16 bytes");

        let msg = {
            let mut input = [0u8; 16];
            input[..len].copy_from_slice(msg);
            input
        };
        // Interpret msg as number.
        let (mut lower, mut upper) = bytes_to_limbs(&msg);

        if len > 8 {
            upper[len - 8] |= 0b1000000;
        } else {
            lower[len] |= 1;
        }

        let n0 = Scalar::from_bits(lower);
        let n1 = Scalar::from_bits(upper);

        // Add to accumulator
        self.accumulator.0 = self.accumulator.0.clone() + n0;
        self.accumulator.1 = self.accumulator.1.clone() + n1;
        if let Some((ref mut a0, ref mut a1)) = self.accumulator_witness {
            *a0 += n0;
            *a1 += n1;
        }
        self.mul_r_mod_p(cs)?;

        Ok(())
    }

    pub fn finalize_and_constrain<CS: ConstraintSystem + ?Sized>(
        self,
        cs: &mut CS,
        msg: &[u8],
        tag: &[u8],
    ) -> Result<(), Error> {
        ensure!(tag.len() == 16, "TAG length should be 16 bytes");

        let wire = self.finalize(cs, msg)?;

        let mut bytes = [0u8; 32];
        bytes[0..16].copy_from_slice(tag);
        let tag = Scalar::from_bits(bytes);
        cs.constrain(wire - tag);

        Ok(())
    }

    /// Returns the allocation of the tag as a single number.
    ///
    /// If no final bytes remain, pass `&[]` to `msg`.
    pub fn finalize<CS: ConstraintSystem + ?Sized>(
        mut self,
        cs: &mut CS,
        msg: &[u8],
    ) -> Result<LinearCombination, Error> {
        // TODO: maybe it's useful to accept msg > 16 bytes here, and chunk it to absorb_partial.
        if msg.len() > 0 {
            self.absorb_partial(cs, msg)?;
        }
        self.reduce_mod_p(cs)?;

        Ok(self.accumulator.0 + self.accumulator.1 * Scalar::from(1u128 << 65) + self.s)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Test code based on
    // `https://github.com/cesarb/chacha20-poly1305-aead/blob/master/src/poly1305.rs`
    static TEXT: &'static [u8] = b"\
        Any submission to the IETF intended by the Contributor for publi\
        cation as all or part of an IETF Internet-Draft or RFC and any s\
        tatement made within the context of an IETF activity is consider\
        ed an \"IETF Contribution\". Such statements include oral statemen\
        ts in IETF sessions, as well as written and electronic communica\
        tions made at any time or place, which are addressed to";

    macro_rules! impl_test_vector {
        ($name:ident, $key_witness:expr, $msg:expr, $tag:expr) => {
            #[test]
            fn $name() -> Result<(), Error> {
                let key_witness = $key_witness;
                let key: Vec<_> = key_witness
                    .iter()
                    .flat_map(|b| {
                        (0..8).map(move |bit| {
                            let bit = (*b >> bit) & 1;
                            LinearCombination::from(Scalar::from(bit as u32))
                        })
                    })
                    .collect();

                let tag = $tag;

                prove_and_verify(|cs| {
                    let mut msg: &[u8] = &$msg;

                    let mut state = Poly1305Gadget::new(&key, Some(&key_witness))?;
                    while msg.len() >= 16 {
                        state.absorb_block(cs, &msg[..16])?;
                        msg = &msg[16..];
                    }
                    state.finalize_and_constrain(cs, msg, &tag)
                })
            }
        };
    }

    impl_test_vector!(zero_test, [0u8; 32], [0u8; 16 * 4], [0u8; 16]);
    impl_test_vector!(
        test_vector_2,
        [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x36, 0xe5, 0xf6, 0xb5, 0xc5, 0xe0, 0x60, 0x70, 0xf0, 0xef, 0xca, 0x96,
            0x22, 0x7a, 0x86, 0x3e,
        ],
        TEXT,
        [
            0x36, 0xe5, 0xf6, 0xb5, 0xc5, 0xe0, 0x60, 0x70, 0xf0, 0xef, 0xca, 0x96, 0x22, 0x7a,
            0x86, 0x3e,
        ]
    );
}
