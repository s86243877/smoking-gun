//! Implementation of RFC7539 against Bulletproofs
//!
//! Decryption is *lazy*, because it's expensive.

use crate::aead::chacha::{self, *};
#[allow(unused)]
use crate::aead::poly1305::*;

use bulletproofs::r1cs::{ConstraintSystem, LinearCombination, R1CSError};

pub struct OpeningGadget {
    key: [LinearCombination; chacha::KEY_LEN],
    key_witness: Option<[u8; chacha::KEY_LEN]>,
}

/// Evaluates to 0 when the supplied key material and tag authenticated the ciphertext.
///
/// This must either be constrained, or used in further computation for composition of a proof.
#[must_use = "Key validity must be constrained!"]
pub struct KeyValidity(LinearCombination);

impl std::ops::Deref for KeyValidity {
    type Target = LinearCombination;
    fn deref(&self) -> &LinearCombination {
        &self.0
    }
}

impl AsRef<LinearCombination> for KeyValidity {
    fn as_ref(&self) -> &LinearCombination {
        &self.0
    }
}

impl KeyValidity {
    pub fn constrain<CS: ConstraintSystem + ?Sized>(self, cs: &mut CS) {
        cs.constrain(self.0);
    }
}

pub struct OpenedStream<'a> {
    inner: ChaCha20Stream,
    ciphertext: &'a [u8],
}

#[doc(hidden)]
pub mod doctest_data {
    pub const CIPHERTEXT: &[u8] = &[
        0xd3, 0x1a, 0x8d, 0x34, 0x64, 0x8e, 0x60, 0xdb, 0x7b, 0x86, 0xaf, 0xbc, 0x53, 0xef, 0x7e,
        0xc2, 0xa4, 0xad, 0xed, 0x51, 0x29, 0x6e, 0x08, 0xfe, 0xa9, 0xe2, 0xb5, 0xa7, 0x36, 0xee,
        0x62, 0xd6, 0x3d, 0xbe, 0xa4, 0x5e, 0x8c, 0xa9, 0x67, 0x12, 0x82, 0xfa, 0xfb, 0x69, 0xda,
        0x92, 0x72, 0x8b, 0x1a, 0x71, 0xde, 0x0a, 0x9e, 0x06, 0x0b, 0x29, 0x05, 0xd6, 0xa5, 0xb6,
        0x7e, 0xcd, 0x3b, 0x36, 0x92, 0xdd, 0xbd, 0x7f, 0x2d, 0x77, 0x8b, 0x8c, 0x98, 0x03, 0xae,
        0xe3, 0x28, 0x09, 0x1b, 0x58, 0xfa, 0xb3, 0x24, 0xe4, 0xfa, 0xd6, 0x75, 0x94, 0x55, 0x85,
        0x80, 0x8b, 0x48, 0x31, 0xd7, 0xbc, 0x3f, 0xf4, 0xde, 0xf0, 0x8e, 0x4b, 0x7a, 0x9d, 0xe5,
        0x76, 0xd2, 0x65, 0x86, 0xce, 0xc6, 0x4b, 0x61, 0x16, 0x1a, 0xe1, 0x0b, 0x59, 0x4f, 0x09,
        0xe2, 0x6a, 0x7e, 0x90, 0x2e, 0xcb, 0xd0, 0x60, 0x06, 0x91,
    ]; // encrypted + tag

    pub const KEY: [u8; 32] = [
        0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e,
        0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d,
        0x9e, 0x9f,
    ];

    pub const NONCE: [u8; 12] = [
        0x07, 0x00, 0x00, 0x00, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47,
    ];

    pub const PLAINTEXT: &[u8] = b"Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the future, sunscreen would be it.";
}

impl OpenedStream<'_> {
    /// Pseudo "iterator" for block-sized decryptions.
    ///
    /// # Example
    ///
    /// ```rust
    /// use smoking_gun::aead::chacha20poly1305::OpeningGadget;
    /// # let key = smoking_gun::aead::chacha20poly1305::doctest_data::KEY;
    /// # let nonce = smoking_gun::aead::chacha20poly1305::doctest_data::NONCE;
    /// # let plaintext = smoking_gun::aead::chacha20poly1305::doctest_data::PLAINTEXT;
    /// # let ciphertext = smoking_gun::aead::chacha20poly1305::doctest_data::CIPHERTEXT;
    /// # smoking_gun::utils::prove_and_verify(|cs| {
    ///
    /// let opener = OpeningGadget::new(cs, Some(key))?;
    /// let (validity, mut stream) = opener.open(cs, nonce, &[], &ciphertext)?;
    /// validity.constrain(cs);
    ///
    /// // A known-answer test
    /// let mut kat: &[u8] = plaintext;
    ///
    /// while let Some(plaintext) = stream.next_block(cs).transpose()? {
    ///     for byte in plaintext {
    ///         cs.constrain(byte - kat[0]);
    ///         kat = &kat[1..];
    ///     }
    /// }
    /// assert_eq!(0, kat.len());
    ///
    /// # Ok(())
    /// # }).expect("valid proof")
    /// ```
    pub fn next_block<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
    ) -> Option<Result<Vec<LinearCombination>, R1CSError>> {
        if self.ciphertext.len() == 0 {
            return None;
        }
        let len = std::cmp::min(BLOCK_LEN, self.ciphertext.len());
        let decrypted = self.inner.apply_keystream(cs, &self.ciphertext[0..len]);
        self.ciphertext = &self.ciphertext[len..];
        Some(decrypted)
    }

    /// Collect the blocks into a container.
    ///
    /// Panics where `next_block()` would return an error.
    ///
    /// # Example
    ///
    /// ```rust
    /// use smoking_gun::aead::chacha20poly1305::OpeningGadget;
    /// # let key = smoking_gun::aead::chacha20poly1305::doctest_data::KEY;
    /// # let nonce = smoking_gun::aead::chacha20poly1305::doctest_data::NONCE;
    /// # let plaintext = smoking_gun::aead::chacha20poly1305::doctest_data::PLAINTEXT;
    /// # let ciphertext = smoking_gun::aead::chacha20poly1305::doctest_data::CIPHERTEXT;
    /// # smoking_gun::utils::prove_and_verify(|cs| {
    ///
    /// let opener = OpeningGadget::new(cs, Some(key))?;
    /// let (validity, mut stream) = opener.open(cs, nonce, &[], &ciphertext)?;
    /// validity.constrain(cs);
    ///
    /// // A known-answer test
    /// let mut kat: &[u8] = plaintext;
    ///
    /// let plaintext_wires: Vec<_> = stream.collect(cs);
    /// for (byte, known_byte) in plaintext_wires.into_iter().zip(kat.iter()) {
    ///     cs.constrain(byte - *known_byte);
    /// }
    ///
    /// # Ok(())
    /// # }).expect("valid proof")
    /// ```
    pub fn collect<CS: ConstraintSystem + ?Sized, B>(mut self, cs: &mut CS) -> B
    where
        B: std::iter::FromIterator<LinearCombination>,
    {
        let iter = (0..((self.ciphertext.len() + BLOCK_LEN - 1) / BLOCK_LEN))
            .flat_map(|_| self.next_block(cs).unwrap().unwrap().into_iter());
        std::iter::FromIterator::from_iter(iter)
    }
}

impl OpeningGadget {
    /// Injects the supplied key into the proof system for authentication and decryption.
    pub fn new<CS: ConstraintSystem + ?Sized>(
        cs: &mut CS,
        key_witness: Option<[u8; chacha::KEY_LEN]>,
    ) -> Result<Self, R1CSError> {
        // Constrain key bits
        let key = crate::utils::byte_array_witness(cs, key_witness)?;

        Ok(OpeningGadget { key, key_witness })
    }

    /// Authenticates the data and returns a key stream
    pub fn open<'a, CS: ConstraintSystem + ?Sized, B: AsRef<[u8]>>(
        &self,
        cs: &mut CS,
        nonce: [u8; NONCE_LEN],
        aad: B,
        ciphertext: &'a [u8],
    ) -> Result<(KeyValidity, OpenedStream<'a>), R1CSError> {
        let stream = OpenedStream {
            inner: ChaCha20Stream::from_allocated_key(
                self.key.clone(),
                self.key_witness.clone(),
                nonce,
            ),
            ciphertext: &ciphertext[0..(ciphertext.len() - 16)],
        };

        // XXX: implement
        drop(nonce);
        drop(aad);
        drop(cs);

        Ok((KeyValidity(LinearCombination::default()), stream))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use ring::aead::*;
    use ring::rand::{SecureRandom, SystemRandom};

    #[rstest::rstest(plaintext,
        case(b"Hello, world"),
        case(b"Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the future, sunscreen would be it."),
    )]
    fn against_ring(plaintext: &[u8]) -> Result<(), failure::Error> {
        // key setup
        let rng = SystemRandom::new();
        let mut key_bytes = [0u8; 32];
        rng.fill(&mut key_bytes).unwrap();
        let mut nonce_bytes = [0u8; chacha::NONCE_LEN];
        rng.fill(&mut nonce_bytes).unwrap();

        let key = UnboundKey::new(&CHACHA20_POLY1305, &key_bytes).unwrap();
        let key = LessSafeKey::new(key);
        let nonce = Nonce::try_assume_unique_for_key(&nonce_bytes).unwrap();

        let mut ciphertext = Vec::with_capacity(plaintext.len() + TAG_LEN);
        ciphertext.extend(plaintext);
        key.seal_in_place_append_tag(nonce, Aad::empty(), &mut ciphertext)
            .unwrap();

        // Test with blocks
        crate::utils::prove_and_verify(|cs| {
            let opener = OpeningGadget::new(cs, Some(key_bytes))?;
            let (validity, mut stream) = opener.open(cs, nonce_bytes, &[], &ciphertext)?;
            validity.constrain(cs);

            let mut kat: &[u8] = plaintext;

            while let Some(plaintext) = stream.next_block(cs).transpose()? {
                for byte in plaintext {
                    cs.constrain(byte - kat[0]);
                    kat = &kat[1..];
                }
            }
            assert_eq!(0, kat.len());

            Ok(())
        })?;

        // Test with collect()
        crate::utils::prove_and_verify(|cs| {
            let opener = OpeningGadget::new(cs, Some(key_bytes))?;
            let (validity, stream) = opener.open(cs, nonce_bytes, &[], &ciphertext)?;
            validity.constrain(cs);

            let kat: &[u8] = plaintext;

            let plain: Vec<_> = stream.collect(cs);
            assert_eq!(kat.len(), plain.len());
            for (byte, kat) in plain.into_iter().zip(kat.iter()) {
                cs.constrain(byte - *kat);
            }

            Ok(())
        })
    }

    #[test]
    fn rfc7539_example_2_8_2() -> Result<(), failure::Error> {
        // Plaintext:
        // 000  4c 61 64 69 65 73 20 61 6e 64 20 47 65 6e 74 6c  Ladies and Gentl
        // 016  65 6d 65 6e 20 6f 66 20 74 68 65 20 63 6c 61 73  emen of the clas
        // 032  73 20 6f 66 20 27 39 39 3a 20 49 66 20 49 20 63  s of '99: If I c
        // 048  6f 75 6c 64 20 6f 66 66 65 72 20 79 6f 75 20 6f  ould offer you o
        // 064  6e 6c 79 20 6f 6e 65 20 74 69 70 20 66 6f 72 20  nly one tip for
        // 080  74 68 65 20 66 75 74 75 72 65 2c 20 73 75 6e 73  the future, suns
        // 096  63 72 65 65 6e 20 77 6f 75 6c 64 20 62 65 20 69  creen would be i
        // 112  74 2e                                            t.
        //
        // AAD:
        // 000  50 51 52 53 c0 c1 c2 c3 c4 c5 c6 c7              PQRS........
        //
        // Key:
        // 000  80 81 82 83 84 85 86 87 88 89 8a 8b 8c 8d 8e 8f  ................
        // 016  90 91 92 93 94 95 96 97 98 99 9a 9b 9c 9d 9e 9f  ................
        //
        // IV:
        // 000  40 41 42 43 44 45 46 47                          @ABCDEFG
        //
        // 32-bit fixed-common part:
        // 000  07 00 00 00                                      ....
        //
        // AEAD Construction for Poly1305:
        // 000  50 51 52 53 c0 c1 c2 c3 c4 c5 c6 c7 00 00 00 00  PQRS............
        // 016  d3 1a 8d 34 64 8e 60 db 7b 86 af bc 53 ef 7e c2  ...4d.`.{...S.~.
        // 032  a4 ad ed 51 29 6e 08 fe a9 e2 b5 a7 36 ee 62 d6  ...Q)n......6.b.
        // 048  3d be a4 5e 8c a9 67 12 82 fa fb 69 da 92 72 8b  =..^..g....i..r.
        // 064  1a 71 de 0a 9e 06 0b 29 05 d6 a5 b6 7e cd 3b 36  .q.....)....~.;6
        // 080  92 dd bd 7f 2d 77 8b 8c 98 03 ae e3 28 09 1b 58  ....-w......(..X
        // 096  fa b3 24 e4 fa d6 75 94 55 85 80 8b 48 31 d7 bc  ..$...u.U...H1..
        // 112  3f f4 de f0 8e 4b 7a 9d e5 76 d2 65 86 ce c6 4b  ?....Kz..v.e...K
        // 128  61 16 00 00 00 00 00 00 00 00 00 00 00 00 00 00  a...............
        // 144  0c 00 00 00 00 00 00 00 72 00 00 00 00 00 00 00  ........r.......
        let ciphertext = [
            0xd3, 0x1a, 0x8d, 0x34, 0x64, 0x8e, 0x60, 0xdb, 0x7b, 0x86, 0xaf, 0xbc, 0x53, 0xef,
            0x7e, 0xc2, 0xa4, 0xad, 0xed, 0x51, 0x29, 0x6e, 0x08, 0xfe, 0xa9, 0xe2, 0xb5, 0xa7,
            0x36, 0xee, 0x62, 0xd6, 0x3d, 0xbe, 0xa4, 0x5e, 0x8c, 0xa9, 0x67, 0x12, 0x82, 0xfa,
            0xfb, 0x69, 0xda, 0x92, 0x72, 0x8b, 0x1a, 0x71, 0xde, 0x0a, 0x9e, 0x06, 0x0b, 0x29,
            0x05, 0xd6, 0xa5, 0xb6, 0x7e, 0xcd, 0x3b, 0x36, 0x92, 0xdd, 0xbd, 0x7f, 0x2d, 0x77,
            0x8b, 0x8c, 0x98, 0x03, 0xae, 0xe3, 0x28, 0x09, 0x1b, 0x58, 0xfa, 0xb3, 0x24, 0xe4,
            0xfa, 0xd6, 0x75, 0x94, 0x55, 0x85, 0x80, 0x8b, 0x48, 0x31, 0xd7, 0xbc, 0x3f, 0xf4,
            0xde, 0xf0, 0x8e, 0x4b, 0x7a, 0x9d, 0xe5, 0x76, 0xd2, 0x65, 0x86, 0xce, 0xc6, 0x4b,
            0x61, 0x16, 0x1a, 0xe1, 0x0b, 0x59, 0x4f, 0x09, 0xe2, 0x6a, 0x7e, 0x90, 0x2e, 0xcb,
            0xd0, 0x60, 0x06, 0x91,
        ]; // encrypted + tag
        println!("ciphertext len: {}", ciphertext.len());

        let key = [
            0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d,
            0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b,
            0x9c, 0x9d, 0x9e, 0x9f,
        ];

        let nonce = [
            0x07, 0x00, 0x00, 0x00, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47,
        ];

        let plaintext = b"Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the future, sunscreen would be it.";
        println!("Plaintext len: {}", plaintext.len());

        crate::utils::prove_and_verify(|cs| {
            let opener = OpeningGadget::new(cs, Some(key))?;
            let (validity, mut stream) = opener.open(cs, nonce, &[], &ciphertext)?;

            let mut kat: &[u8] = plaintext;

            while let Some(plaintext) = stream.next_block(cs).transpose()? {
                for byte in plaintext {
                    cs.constrain(byte - kat[0]);
                    kat = &kat[1..];
                }
            }
            assert_eq!(0, kat.len());
            validity.constrain(cs);

            Ok(())
        })
    }
}
