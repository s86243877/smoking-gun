//! ChaCha20 implementation for Bulletproofs arithmetic circuit use.
//!
//! Use cases:
//! - Prove a property of the plaintext, i.e., given the ciphertext and tag, prove knowledge of the
//!   encryption key s.t. the decryption admits to some properties.  The ChaCha20 takes a `key` and
//!   `nonce` as witness data, and transforms an `[u8; N + TAG_LEN]` slice into a slice of
//!   plaintext bytes `[LinearCombination; N]`.

use crate::utils::*;

use bulletproofs::r1cs::*;
use curve25519_dalek::scalar::Scalar;
use failure::*;

const CHACHA_INIT: [u32; 4] = [0x61707865, 0x3320646e, 0x79622d32, 0x6b206574];

pub const KEY_LEN: usize = 32;
pub const BLOCK_LEN: usize = 64;
pub const TAG_LEN: usize = 16;
pub const NONCE_LEN: usize = 12;

pub struct ChaCha20Stream {
    key: [LinearCombination; KEY_LEN],
    nonce: [u8; NONCE_LEN],
    key_witness: Option<[u8; KEY_LEN]>,
    seek: usize,
}

impl ChaCha20Stream {
    /// Constrains the key witness and nonce witness data to the constraint system.
    /// The resulting `ChaCha20Stream` can be used to constrain encrypted data to plain text.
    pub fn new<CS: ConstraintSystem + ?Sized>(
        cs: &mut CS,
        key_witness: Option<[u8; KEY_LEN]>,
        nonce: [u8; NONCE_LEN],
    ) -> Result<Self, Error> {
        // Constrain key bits
        let key = crate::utils::byte_array_witness(cs, key_witness)?;

        Ok(ChaCha20Stream {
            key,
            nonce,
            key_witness,
            seek: 0,
        })
    }

    /// Constructor that allows for key reuse.
    pub fn from_allocated_key(
        key: [LinearCombination; KEY_LEN],
        key_witness: Option<[u8; KEY_LEN]>,
        nonce: [u8; NONCE_LEN],
    ) -> Self {
        ChaCha20Stream {
            key,
            nonce,
            key_witness,
            seek: 0,
        }
    }

    pub fn prove_preimage<CS: ConstraintSystem + ?Sized>(
        cs: &mut CS,
        key_witness: Option<[u8; KEY_LEN]>,
        nonce: [u8; NONCE_LEN],
        ciphertext: &[u8],
        plaintext_witness: Option<&[u8]>,
    ) -> Result<Vec<LinearCombination>, Error> {
        let mut stream = ChaCha20Stream::new(cs, key_witness, nonce)?;
        let plaintext_witness = byte_slice_witness(cs, ciphertext.len(), plaintext_witness)?;
        let plaintext_wires = stream.apply_keystream(cs, &ciphertext)?;

        use itertools::Itertools;

        // wires are yielded as u8's, so we can chunk them per 248 bit = 31 bytes
        let chunks = plaintext_witness
            .into_iter()
            .zip(plaintext_wires.iter())
            .chunks(31);
        for chunk in chunks.into_iter() {
            let mut l = LinearCombination::default();
            let mut r = LinearCombination::default();
            let mut base = Scalar::zero();
            for (witness, wire) in chunk {
                l = l + base * witness;
                r = r + base * wire.clone();
                base = base * Scalar::from(8u32);
            }
            cs.constrain(l - r);
        }

        Ok(plaintext_wires)
    }

    /// Applies the keystream represented by this ChaChaStream to the supplied [u8].
    /// The Vec returned contains wires representing bytes in the same order.
    pub fn apply_keystream<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
        mut cipherstream: &[u8],
    ) -> Result<Vec<LinearCombination>, R1CSError> {
        use std::collections::VecDeque;

        let mut out = Vec::with_capacity(cipherstream.len());

        let mut bitstream: VecDeque<LinearCombination> = VecDeque::new();

        while cipherstream.len() > 0 {
            if self.seek % BLOCK_LEN == 0 {
                // Block boundaries
                // Request next block
                let mut block = self.state();
                let orig = block.clone();
                block.rounds(cs)?;
                block.add(cs, orig)?;
                bitstream = block.as_bitstream(cs)?.into();

                // TODO: skip bytes for unaligned application.
            }
            debug_assert!(bitstream.len() >= 8);

            let cipher_byte = cipherstream[0];
            let mut plain_byte = LinearCombination::default();
            for bit in 0..8 {
                let xor = if cipher_byte & (1 << bit) == 0 {
                    // 0 xor bitstream == bitstream
                    LinearCombination::from(bitstream.pop_front().unwrap())
                } else {
                    // 1 xor bitstream == 1 - bitstream
                    -bitstream.pop_front().unwrap() + 1u32
                };
                plain_byte = plain_byte + xor * (1u32 << bit);
            }
            out.push(plain_byte);

            self.seek += 1;
            cipherstream = &cipherstream[1..];
        }

        Ok(out)
    }

    fn block(&self) -> u32 {
        (self.seek / BLOCK_LEN + 1) as u32
    }

    pub fn seek(&mut self, seek: usize) {
        self.seek = seek;
    }

    pub(crate) fn state(&self) -> ChaChaState {
        self.state_with_block(self.block())
    }

    pub(crate) fn state_with_block(&self, block: u32) -> ChaChaState {
        let mut state = ChaChaState::default();

        // cccccccc cccccccc cccccccc cccccccc
        state.state[0] = [
            CHACHA_INIT[0].into(),
            CHACHA_INIT[1].into(),
            CHACHA_INIT[2].into(),
            CHACHA_INIT[3].into(),
        ];

        // kkkkkkkk kkkkkkkk kkkkkkkk kkkkkkkk
        // kkkkkkkk kkkkkkkk kkkkkkkk kkkkkkkk
        for row in 1..=2 {
            for word in 0..4 {
                let offset = (word + (row - 1) * 4) * 4;
                let block = self.key[offset + 0].clone()
                    + self.key[offset + 1].clone() * (1u32 << 8)
                    + self.key[offset + 2].clone() * (1u32 << 16)
                    + self.key[offset + 3].clone() * (1u32 << 24);
                state[(row, word)] = block.into();
            }
        }

        // bbbbbbbb
        state[(3, 0)] = block.into();

        //          nnnnnnnn nnnnnnnn nnnnnnnn
        for word in 1..4 {
            let offset = (word - 1) * 4;
            state[(3, word)] = (self.nonce[offset + 0] as u32
                + self.nonce[offset + 1] as u32 * (1u32 << 8)
                + self.nonce[offset + 2] as u32 * (1u32 << 16)
                + self.nonce[offset + 3] as u32 * (1u32 << 24))
                .into();
        }

        // Append witness if applicable
        match self.key_witness.as_ref() {
            None => (),
            Some(key_witness) => {
                let mut witness = [[0u32; 4]; 4];
                // cccccccc cccccccc cccccccc cccccccc
                witness[0] = CHACHA_INIT;
                // kkkkkkkk kkkkkkkk kkkkkkkk kkkkkkkk
                // kkkkkkkk kkkkkkkk kkkkkkkk kkkkkkkk
                for row in 1..=2 {
                    for word in 0..4 {
                        let offset = (word + (row - 1) * 4) * 4;
                        witness[row][word] = key_witness[offset + 0] as u32
                            + key_witness[offset + 1] as u32 * (1u32 << 8)
                            + key_witness[offset + 2] as u32 * (1u32 << 16)
                            + key_witness[offset + 3] as u32 * (1u32 << 24);
                    }
                }
                // bbbbbbbb
                witness[3][0] = block;
                //          nnnnnnnn nnnnnnnn nnnnnnnn
                for word in 1..4 {
                    let offset = (word - 1) * 4;
                    witness[3][word] = self.nonce[offset + 0] as u32
                        + self.nonce[offset + 1] as u32 * (1u32 << 8)
                        + self.nonce[offset + 2] as u32 * (1u32 << 16)
                        + self.nonce[offset + 3] as u32 * (1u32 << 24);
                }

                state.witness = Some(witness);
            }
        }

        state
    }
}

#[derive(Clone)]
pub(crate) enum ChaChaBlock {
    Bits([LinearCombination; 32]),
    U32(LinearCombination),
    Both(LinearCombination, [LinearCombination; 32]),
}

impl ChaChaBlock {
    pub(crate) fn as_number(&mut self) -> LinearCombination {
        match self {
            ChaChaBlock::U32(val) | ChaChaBlock::Both(val, _) => val.clone(),
            ChaChaBlock::Bits(bits) => {
                let sum = bits
                    .iter()
                    .enumerate()
                    .map(|(i, bit): (usize, &LinearCombination)| {
                        bit.clone() * Scalar::from(1u32 << i)
                    })
                    .fold(LinearCombination::default(), |a, b| a + b);
                *self = ChaChaBlock::Both(sum.clone(), (*bits).clone());
                sum
            }
        }
    }

    fn unwrap_as_bits(&self) -> &[LinearCombination; 32] {
        match self {
            ChaChaBlock::Bits(bits) => bits,
            ChaChaBlock::Both(_, bits) => bits,
            ChaChaBlock::U32(_num) => panic!("unwrap_as_bits"),
        }
    }

    pub(crate) fn as_bits<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
        witness: Option<u32>,
    ) -> Result<&[LinearCombination; 32], R1CSError> {
        match self {
            ChaChaBlock::U32(num) => {
                let (sum, bits) = u32_into_bits::<_, 32>(cs, witness)?;
                cs.constrain(sum.clone() - num.clone());
                *self = ChaChaBlock::Both(sum, bits);
                Ok(self.unwrap_as_bits())
            }
            ChaChaBlock::Bits(bits) => Ok(bits),
            ChaChaBlock::Both(_, bits) => Ok(bits),
        }
    }

    pub(crate) fn into_number(self) -> LinearCombination {
        match self {
            ChaChaBlock::U32(val) | ChaChaBlock::Both(val, _) => val,
            ChaChaBlock::Bits(bits) => bits
                .iter()
                .enumerate()
                .map(|(i, bit): (usize, &LinearCombination)| bit.clone() * Scalar::from(1u32 << i))
                .fold(LinearCombination::default(), |a, b| a + b),
        }
    }
}

impl core::ops::SubAssign<ChaChaBlock> for ChaChaBlock {
    fn sub_assign(&mut self, mut other: Self) {
        *self = ChaChaBlock::U32(self.as_number() - other.as_number());
    }
}

impl core::ops::Add<ChaChaBlock> for ChaChaBlock {
    type Output = ChaChaBlock;

    fn add(self, other: Self) -> Self::Output {
        (self.into_number() + other.into_number()).into()
    }
}

impl core::ops::Sub<ChaChaBlock> for ChaChaBlock {
    type Output = ChaChaBlock;

    fn sub(self, other: Self) -> Self::Output {
        (self.into_number() - other.into_number()).into()
    }
}

impl From<u32> for ChaChaBlock {
    fn from(s: u32) -> ChaChaBlock {
        let sum = s.into();
        let mut bits = zero_array();
        for (i, bit) in bits.iter_mut().enumerate() {
            if s & (1 << i) != 0 {
                *bit = Scalar::one().into();
            }
        }

        ChaChaBlock::Both(sum, bits)
    }
}

impl From<LinearCombination> for ChaChaBlock {
    fn from(lc: LinearCombination) -> ChaChaBlock {
        ChaChaBlock::U32(lc)
    }
}

impl Default for ChaChaBlock {
    fn default() -> ChaChaBlock {
        ChaChaBlock::Both(Default::default(), zero_array())
    }
}

#[derive(Clone, Default)]
pub(crate) struct ChaChaState {
    /// The block structure.
    /// First index is row, second index is column.
    state: [[ChaChaBlock; 4]; 4],
    witness: Option<[[u32; 4]; 4]>,
}

impl core::ops::IndexMut<usize> for ChaChaState {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        &mut self.state[i / 4][i % 4]
    }
}

impl core::ops::IndexMut<(usize, usize)> for ChaChaState {
    fn index_mut(&mut self, (i, j): (usize, usize)) -> &mut Self::Output {
        &mut self.state[i][j]
    }
}

impl core::ops::Index<usize> for ChaChaState {
    type Output = ChaChaBlock;
    fn index(&self, i: usize) -> &Self::Output {
        &self.state[i / 4][i % 4]
    }
}

impl core::ops::Index<(usize, usize)> for ChaChaState {
    type Output = ChaChaBlock;
    fn index(&self, (i, j): (usize, usize)) -> &Self::Output {
        &self.state[i][j]
    }
}

impl ChaChaState {
    /// Create a ChaCha state for use in tests.
    #[cfg(test)]
    fn from_constants(ct: [[u32; 4]; 4]) -> Self {
        let mut state = ChaChaState::default();
        for i in 0..4 {
            for j in 0..4 {
                state[(i, j)] = ct[i][j].into();
            }
        }

        state.witness = Some(ct);

        state
    }

    fn qr<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
        a: usize,
        b: usize,
        c: usize,
        d: usize,
    ) -> Result<(), R1CSError> {
        self.axr(cs, a, b, d, 16)?;
        self.axr(cs, c, d, b, 12)?;
        self.axr(cs, a, b, d, 8)?;
        self.axr(cs, c, d, b, 7)?;
        Ok(())
    }

    fn rounds<CS: ConstraintSystem + ?Sized>(&mut self, cs: &mut CS) -> Result<(), R1CSError> {
        for _ in 0..10 {
            for column in 0..4 {
                self.qr(cs, column, column + 4, column + 8, column + 12)?;
            }
            self.qr(cs, 0, 5, 10, 15)?;
            self.qr(cs, 1, 6, 11, 12)?;
            self.qr(cs, 2, 7, 8, 13)?;
            self.qr(cs, 3, 4, 9, 14)?;
        }
        Ok(())
    }

    /// Perform a 16th round:
    /// x += y; z ^= x; z <<<= rot
    fn axr<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
        x: usize,
        y: usize,
        z: usize,
        rot: usize,
    ) -> Result<(), R1CSError> {
        macro_rules! with_witness {
            ($idxmut:ident $(, $idx:ident)* ;$e:expr) => {
                match self.witness.as_mut() {
                    Some(state) => {
                        let res = {
                            let $idxmut = state[$idxmut / 4][$idxmut % 4];
                            $(let $idx = state[$idx / 4][$idx % 4];)*
                            $e
                        };
                        state[$idxmut / 4][$idxmut % 4] = res;
                    }
                    None => (),
                }
            };
        }

        // Step 1: ADD
        self[x] = self[x].clone() + self[y].clone();
        // compute and allocate overflow
        let x_overflow = allocate_bit(
            cs,
            self.witness
                .as_ref()
                .map(|w| w[x / 4][x % 4].checked_add(w[y / 4][y % 4]).is_none()),
        )?;
        with_witness!(x, y; x.wrapping_add(y));

        // It is allowed to overflow x = x + y with one bit.
        self[x] -= (Scalar::from(1u64 << 32) * x_overflow).into();

        // We further only constrain bits 0..32.
        // This effectively simulates u32 in an arithmetic circuit [citation needed],
        // and `z` will come out as a 32 bit number.
        // We will further reconstruct `x` from its bits, and as such complete the modulo
        // operation.
        let x_witness = self.witness.as_ref().map(|w| w[x / 4][x % 4]);
        let x_bits = self[x].as_bits(cs, x_witness)?.clone();

        let z_witness = self.witness.as_ref().map(|w| w[z / 4][z % 4]);
        let z_bits = self[z].as_bits(cs, z_witness)?.clone();

        // z ^= x ; z <<<= rot
        let mut z_result = zero_array::<32>();
        for bit in 0..32 {
            let sum = x_bits[bit].clone() + z_bits[bit].clone();
            let (_, _, prod) = cs.multiply(x_bits[bit].clone(), z_bits[bit].clone());
            let z_xor = sum - Scalar::from(2u32) * prod;

            z_result[(bit + rot) % 32] = z_xor;
        }

        self[z] = ChaChaBlock::Bits(z_result);

        // Finish the witness transform
        with_witness!(z, x; z ^ x);
        with_witness!(z; z.rotate_left(rot as u32));

        Ok(())
    }

    fn add<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
        other: Self,
    ) -> Result<(), R1CSError> {
        for i in 0..4 {
            for j in 0..4 {
                // TODO: MOD 1<<32

                let overflow_bit = match (self.witness.as_mut(), other.witness.as_ref()) {
                    (Some(witness), Some(other_witness)) => {
                        let (o, f) = witness[i][j].overflowing_add(other_witness[i][j]);
                        witness[i][j] = o;
                        Some(f)
                    }
                    (Some(_), None) | (None, Some(_)) => {
                        unreachable!("Partial witness not supported")
                    }
                    (None, None) => None,
                };

                let overflow_bit = allocate_bit(cs, overflow_bit)? * (1u64 << 32);

                self[(i, j)] = self[(i, j)].clone() + other[(i, j)].clone() - overflow_bit.into();
            }
        }
        Ok(())
    }

    /// Bits for the keystream in LittleEndian order.
    pub(crate) fn as_bitstream<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
    ) -> Result<Vec<LinearCombination>, R1CSError> {
        let mut bitstream = Vec::with_capacity(4 * 4 * 32);

        for i in 0..4 {
            for j in 0..4 {
                let witness = self.witness.as_ref().map(|w| w[i][j]);
                let bits = self[(i, j)].as_bits(cs, witness)?;
                bitstream.extend(bits.iter().map(Clone::clone));
            }
        }

        Ok(bitstream)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn bind_state_to_plain<CS: ConstraintSystem + ?Sized>(
        cs: &mut CS,
        state: &mut ChaChaState,
        plain: &[[u32; 4]; 4],
    ) -> Result<(), Error> {
        for i in 0..4 {
            for j in 0..4 {
                if let Some(w) = state.witness.as_ref() {
                    assert_eq!(
                        w[i][j], plain[i][j],
                        "Inequality in witness({:x})==plain({:x}) at {},{}",
                        w[i][j], plain[i][j], i, j
                    );
                }
                cs.constrain(state[(i, j)].as_number() - plain[i][j]);
            }
        }
        Ok(())
    }

    const RFC_7539_KEY: [u8; KEY_LEN] = [
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
        0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
        0x1e, 0x1f,
    ];

    #[test]
    fn generate_state_from_key() -> Result<(), Error> {
        let nonce: [u8; NONCE_LEN] = [
            0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x4a, 0x00, 0x00, 0x00, 0x00,
        ];

        prove_and_verify(|cs| {
            let stream = ChaCha20Stream::new(cs, Some(RFC_7539_KEY), nonce)?;
            let mut state = stream.state();

            bind_state_to_plain(
                cs,
                &mut state,
                &[
                    [0x61707865, 0x3320646e, 0x79622d32, 0x6b206574],
                    [0x03020100, 0x07060504, 0x0b0a0908, 0x0f0e0d0c],
                    [0x13121110, 0x17161514, 0x1b1a1918, 0x1f1e1d1c],
                    [0x00000001, 0x09000000, 0x4a000000, 0x00000000],
                ],
            )?;

            Ok(())
        })
    }

    #[test]
    /// Test axr function, with the test data from RFC 7539 (example in sec 2.1).
    fn qqr() -> Result<(), Error> {
        prove_and_verify(|cs| {
            let mut state = ChaChaState::from_constants([
                [0x11111111, 0x01020304, 0x77777777, 0x01234567],
                [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                [0x00000000, 0x00000000, 0x00000000, 0x00000000],
            ]);

            state.axr(cs, 2, 3, 1, 7)?;

            bind_state_to_plain(
                cs,
                &mut state,
                &[
                    [0x11111111, 0xcc5fed3c, 0x789abcde, 0x01234567],
                    [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                    [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                    [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                ],
            )?;

            Ok(())
        })
    }

    #[test]
    fn seek_to_block() -> Result<(), Error> {
        let nonce: [u8; NONCE_LEN] = [
            0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x4a, 0x00, 0x00, 0x00, 0x00,
        ];

        prove_and_verify(|cs| {
            let mut stream = ChaCha20Stream::new(cs, Some(RFC_7539_KEY), nonce)?;

            // seek 0 gives block 1
            assert_eq!(stream.block(), 1);
            stream.seek(64);
            assert_eq!(stream.block(), 2);
            stream.seek(63);
            assert_eq!(stream.block(), 1);

            Ok(())
        })
    }

    #[test]
    /// Test a quarter round, with the test data from RFC 7539 (example in sec 2.1.1).
    fn qr_example() -> Result<(), Error> {
        prove_and_verify(|cs| {
            let mut state = ChaChaState::from_constants([
                [0x11111111, 0x01020304, 0x9b8d6f43, 0x01234567],
                [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                [0x00000000, 0x00000000, 0x00000000, 0x00000000],
            ]);

            state.qr(cs, 0, 1, 2, 3)?;

            bind_state_to_plain(
                cs,
                &mut state,
                &[
                    [0xea2a92f4, 0xcb1cf8ce, 0x4581472e, 0x5881c4bb],
                    [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                    [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                    [0x00000000, 0x00000000, 0x00000000, 0x00000000],
                ],
            )?;

            Ok(())
        })
    }

    #[test]
    /// Test a quarter round 2-7-8-13 on a state, with the test data from RFC 7539 (example in 2.2).
    fn qr2_7_8_13() -> Result<(), Error> {
        prove_and_verify(|cs| {
            let mut state = ChaChaState::from_constants([
                [0x879531e0, 0xc5ecf37d, 0x516461b1, 0xc9a62f8a],
                [0x44c20ef3, 0x3390af7f, 0xd9fc690b, 0x2a5f714c],
                [0x53372767, 0xb00a5631, 0x974c541a, 0x359e9963],
                [0x5c971061, 0x3d631689, 0x2098d9d6, 0x91dbd320],
            ]);

            state.qr(cs, 2, 7, 8, 13)?;

            bind_state_to_plain(
                cs,
                &mut state,
                &[
                    [0x879531e0, 0xc5ecf37d, 0xbdb886dc, 0xc9a62f8a],
                    [0x44c20ef3, 0x3390af7f, 0xd9fc690b, 0xcfacafd2],
                    [0xe46bea80, 0xb00a5631, 0x974c541a, 0x359e9963],
                    [0x5c971061, 0xccc07c79, 0x2098d9d6, 0x91dbd320],
                ],
            )?;

            Ok(())
        })
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore)]
    /// Test after 20 rounds on a state, with the test data from RFC 7539 (example in 2.3.2).
    fn after_20_rounds() -> Result<(), Error> {
        let nonce: [u8; NONCE_LEN] = [
            0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x4a, 0x00, 0x00, 0x00, 0x00,
        ];

        prove_and_verify(|cs| {
            let stream = ChaCha20Stream::new(cs, Some(RFC_7539_KEY), nonce)?;
            let mut state = stream.state();
            state.rounds(cs)?;

            bind_state_to_plain(
                cs,
                &mut state,
                &[
                    [0x837778ab, 0xe238d763, 0xa67ae21e, 0x5950bb2f],
                    [0xc4f2d0c7, 0xfc62bb2f, 0x8fa018fc, 0x3f5ec7b7],
                    [0x335271c2, 0xf29489f3, 0xeabda8fc, 0x82e46ebd],
                    [0xd19c12b4, 0xb04e16de, 0x9e83d0cb, 0x4e3c50a2],
                ],
            )?;

            state.add(cs, stream.state())?;

            bind_state_to_plain(
                cs,
                &mut state,
                &[
                    [0xe4e7f110, 0x15593bd1, 0x1fdd0f50, 0xc47120a3],
                    [0xc7f4d1c7, 0x0368c033, 0x9aaa2204, 0x4e6cd4c3],
                    [0x466482d2, 0x09aa9f07, 0x05d7c214, 0xa2028bd9],
                    [0xd19c12b5, 0xb94e16de, 0xe883d0cb, 0x4e3c50a2],
                ],
            )?;

            Ok(())
        })
    }

    #[test]
    /// Test the blocks specified in 2.4.2 of the RFC
    fn blocks_in_cipher_example() -> Result<(), Error> {
        let nonce: [u8; NONCE_LEN] = [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4a, 0x00, 0x00, 0x00, 0x00,
        ];

        prove_and_verify(|cs| {
            let stream = ChaCha20Stream::new(cs, Some(RFC_7539_KEY), nonce)?;

            let kats = [
                &[
                    [0xf3514f22, 0xe1d91b40, 0x6f27de2f, 0xed1d63b8],
                    [0x821f138c, 0xe2062c3d, 0xecca4f7e, 0x78cff39e],
                    [0xa30a3b8a, 0x920a6072, 0xcd7479b5, 0x34932bed],
                    [0x40ba4c79, 0xcd343ec6, 0x4c2c21ea, 0xb7417df0],
                ],
                &[
                    [0x9f74a669, 0x410f633f, 0x28feca22, 0x7ec44dec],
                    [0x6d34d426, 0x738cb970, 0x3ac5e9f3, 0x45590cc4],
                    [0xda6e8b39, 0x892c831a, 0xcdea67c1, 0x2b7e1d90],
                    [0x037463f3, 0xa11a2073, 0xe8bcfb88, 0xedc49139],
                ],
            ];
            for (i, kat) in kats.iter().enumerate() {
                let block = i + 1;
                let mut state = stream.state_with_block(block as u32);
                let orig = state.clone();
                state.rounds(cs)?;
                state.add(cs, orig)?;

                println!("Constraining block {}", block);
                bind_state_to_plain(cs, &mut state, kat)?;
            }

            Ok(())
        })
    }

    #[test]
    #[cfg_attr(debug_assertions, ignore)]
    fn decrypt() -> Result<(), Error> {
        let nonce: [u8; NONCE_LEN] = [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4a, 0x00, 0x00, 0x00, 0x00,
        ];

        let ciphertext = [
            0x6e, 0x2e, 0x35, 0x9a, 0x25, 0x68, 0xf9, 0x80, 0x41, 0xba, 0x07, 0x28, 0xdd, 0x0d,
            0x69, 0x81, 0xe9, 0x7e, 0x7a, 0xec, 0x1d, 0x43, 0x60, 0xc2, 0x0a, 0x27, 0xaf, 0xcc,
            0xfd, 0x9f, 0xae, 0x0b, 0xf9, 0x1b, 0x65, 0xc5, 0x52, 0x47, 0x33, 0xab, 0x8f, 0x59,
            0x3d, 0xab, 0xcd, 0x62, 0xb3, 0x57, 0x16, 0x39, 0xd6, 0x24, 0xe6, 0x51, 0x52, 0xab,
            0x8f, 0x53, 0x0c, 0x35, 0x9f, 0x08, 0x61, 0xd8, 0x07, 0xca, 0x0d, 0xbf, 0x50, 0x0d,
            0x6a, 0x61, 0x56, 0xa3, 0x8e, 0x08, 0x8a, 0x22, 0xb6, 0x5e, 0x52, 0xbc, 0x51, 0x4d,
            0x16, 0xcc, 0xf8, 0x06, 0x81, 0x8c, 0xe9, 0x1a, 0xb7, 0x79, 0x37, 0x36, 0x5a, 0xf9,
            0x0b, 0xbf, 0x74, 0xa3, 0x5b, 0xe6, 0xb4, 0x0b, 0x8e, 0xed, 0xf2, 0x78, 0x5e, 0x42,
            0x87, 0x4d,
        ];

        let plaintext = b"Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the future, sunscreen would be it.";

        assert_eq!(ciphertext.len(), plaintext.len());

        prove_and_verify(|cs| {
            let mut stream = ChaCha20Stream::new(cs, Some(RFC_7539_KEY), nonce)?;

            let plaintext_witness = byte_slice_witness(cs, plaintext.len(), Some(plaintext))?;
            let plaintext_wires = stream.apply_keystream(cs, &ciphertext)?;

            assert_eq!(plaintext_wires.len(), plaintext_witness.len());
            for (i, (witness, wire)) in plaintext_witness
                .into_iter()
                .zip(plaintext_wires.into_iter())
                .enumerate()
            {
                println!("Constraining byte {}", i);
                cs.constrain(wire - witness);
            }

            Ok(())
        })
    }
}
