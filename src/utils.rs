//! Collection of convenience methods that are used throughout this library,
//! and can be useful for exteral usage.

use std::mem::{self, MaybeUninit};

use bulletproofs::r1cs::*;
use curve25519_dalek::scalar::Scalar;

/// Creates the equivalent of `[LinearCombination::default(); N]`.
///
/// `LinearCombination` does not implement `Copy`; this function efficiently generates a 0-array.
pub fn zero_array<const N: usize>() -> [LinearCombination; N] {
    let mut state: [MaybeUninit<LinearCombination>; N] =
        unsafe { MaybeUninit::uninit().assume_init() };

    for k in state.iter_mut() {
        *k = MaybeUninit::new(LinearCombination::default());
    }

    // FIXME: const_generics  seem to be incompatible with mem::transmute.
    // https://github.com/rust-lang/rust/issues/43408
    unsafe { mem::transmute_copy(&state) }
}

/// Allocates a single bit and returns it as `Variable` for further constraining.
///
/// The prover should supply the bit as witness.
/// This method is at the cost of one multiplicative constraint and two lineair constraints.
pub fn allocate_bit<CS: ConstraintSystem + ?Sized>(
    cs: &mut CS,
    witness: Option<bool>,
) -> Result<Variable, R1CSError> {
    let (bit, bit_neg, bit_mul) = cs.allocate_multiplier(witness.as_ref().map(|w| {
        let b = if *w { 1u32 } else { 0 };
        (b.into(), (1 - b).into())
    }))?;
    cs.constrain(bit_mul.into());
    cs.constrain(bit + bit_neg - 1u32);

    Ok(bit)
}

/// Decomposes the `N` least significant bits of a `u32` into bits.
///
/// The returned result is a tuple `(LinearCombination, [LinearCombination; 32])`.
/// The first element represents the original `u32`, truncated to `N` bits and should still
/// be constrained.
/// The second element are the `N` bits of the `u32` starting at LSB.
pub fn u32_into_bits<CS: ConstraintSystem + ?Sized, const N: usize>(
    cs: &mut CS,
    witness: Option<u32>,
) -> Result<(LinearCombination, [LinearCombination; N]), R1CSError> {
    let mut bits = zero_array();

    for i in 0..N {
        let (bit, bit_neg, bit_mul) = cs.allocate_multiplier(witness.as_ref().map(|w| {
            let b = (w >> i) & 1;
            (b.into(), (1 - b).into())
        }))?;

        cs.constrain(bit_mul.into());
        cs.constrain(bit + bit_neg - 1u32);

        bits[i] = bit.into();
    }

    let sum = bits.iter().enumerate().fold(
        LinearCombination::default(),
        |sum, (i, bit): (usize, &LinearCombination)| sum + bit.clone() * Scalar::from(1u32 << i),
    );

    Ok((sum, bits))
}

/// Takes a public byte slice and outputs a bitwise, LittleEndian interpretation.
pub fn bytes_to_r1cs_bits<'a>(witness: impl IntoIterator<Item = &'a u8>) -> Vec<LinearCombination> {
    witness
        .into_iter()
        .flat_map(|b| {
            (0..8).map(move |bit| {
                let bit = (*b >> bit) & 1;
                LinearCombination::from(Scalar::from(bit as u32))
            })
        })
        .collect()
}

/// Takes a secret byte, fixed-length array and imports and constrains the data to the ConstraintSystem.
///
/// Returns an array of equal size with the byte values represented as `LinearCombination`s.
pub fn byte_array_witness<CS: ConstraintSystem + ?Sized, const N: usize>(
    cs: &mut CS,
    witness: Option<[u8; N]>,
) -> Result<[LinearCombination; N], R1CSError> {
    let mut out = zero_array();

    for byte_i in 0..N {
        let mut byte = LinearCombination::default();
        for bit_i in 0..8 {
            let (bit, neg, mul) = if let Some(witness) = witness.as_ref() {
                let bit = ((witness[byte_i] >> bit_i) & 1) as u32;
                cs.allocate_multiplier(Some((bit.into(), (1 - bit).into())))?
            } else {
                cs.allocate_multiplier(None)?
            };

            cs.constrain(mul.into());
            cs.constrain(bit + neg - 1u32);

            let value: Scalar = (1u32 << bit_i).into();

            byte = byte + value * bit;
        }
        out[byte_i] = byte;
    }

    Ok(out)
}

/// Takes a variable-but-public-length byte slice and constrains the data to the ConstraintSystem.
///
/// Returns a `Vec` of equal size with the byte values represented as `LinearCombination`s.
pub fn byte_slice_witness<CS: ConstraintSystem + ?Sized>(
    cs: &mut CS,
    len: usize,
    witness: Option<&[u8]>,
) -> Result<Vec<LinearCombination>, R1CSError> {
    let mut out = Vec::with_capacity(len);

    for byte_i in 0..len {
        let mut byte = LinearCombination::default();
        for bit_i in 0..8 {
            let (bit, neg, mul) = if let Some(witness) = witness.as_ref() {
                let bit = ((witness[byte_i] >> bit_i) & 1) as u32;
                cs.allocate_multiplier(Some((bit.into(), (1 - bit).into())))?
            } else {
                cs.allocate_multiplier(None)?
            };

            cs.constrain(mul.into());
            cs.constrain(bit + neg - 1u32);

            let value: Scalar = (1u32 << bit_i).into();

            byte = byte + value * bit;
        }
        out.push(byte);
    }

    Ok(out)
}

// XXX: find better way to export this to doctests, but not to outer application.
mod test {
    use bulletproofs::r1cs::{ConstraintSystem, Prover, Verifier};
    use bulletproofs::{BulletproofGens, PedersenGens};
    use failure::*;

    fn init() -> (PedersenGens, merlin::Transcript) {
        let pc_gens = PedersenGens::default();
        let transcript = merlin::Transcript::new(b"chacha20");

        (pc_gens, transcript)
    }

    pub fn prove_and_verify<F>(f: F) -> Result<(), Error>
    where
        F: Fn(&mut dyn ConstraintSystem) -> Result<(), Error>,
    {
        // Prove
        let (proof, bp_gens) = {
            let (pc_gens, mut transcript) = init();
            let mut prover = Prover::new(&pc_gens, &mut transcript);

            f(&mut prover)?;

            let bp_gens = BulletproofGens::new(prover.metrics().multipliers.next_power_of_two(), 1);

            (prover.prove(&bp_gens)?, bp_gens)
        };

        // Verify
        let (pc_gens, mut transcript) = init();
        let mut verifier = Verifier::new(&mut transcript);
        f(&mut verifier)?;

        verifier.verify(&proof, &pc_gens, &bp_gens)?;
        Ok(())
    }
}

// XXX: find better way to export this to doctests, but not to outer application.
#[doc(hidden)]
pub use test::*;
