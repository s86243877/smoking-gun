#![allow(dead_code)]

use std::mem::{self, MaybeUninit};

use crate::utils::*;

use bulletproofs::r1cs::{ConstraintSystem, LinearCombination as LC, R1CSError};
use curve25519_dalek::scalar::Scalar;

/// Initial hash values
const H: [u32; 8] = [
    0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19,
];
/// Round constants for SHA256
const K: [u32; 64] = [
    0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
    0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
    0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
    0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
    0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
    0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2,
];

/// Rotate right operation on an array of LinearCombination.
/// This operation is free.
fn rotate_right<const SIZE: usize>(l: &[LC; SIZE], cnt: usize) -> [LC; SIZE] {
    let mut out = zero_array();

    for i in 0..SIZE {
        out[i] = l[(i + cnt) % SIZE].clone();
    }

    out
}

/// Shift right operation on an array of LinearCombination.
/// This operation is free.
fn shift_right<const SIZE: usize>(l: &[LC; SIZE], cnt: usize) -> [LC; SIZE] {
    let mut out = zero_array();

    for i in 0..SIZE - cnt {
        out[i] = l[i + cnt].clone();
    }

    out
}

/// Not operation on a LinearCombination.
/// This operation is free.
pub(crate) fn not_bit<CS: ConstraintSystem>(l: LC) -> LC {
    -l.clone() + 1u128
}

/// Not operation on an array of LinearCombination.
/// This operation is free.
pub(crate) fn not<const SIZE: usize>(l: &[LC; SIZE]) -> [LC; SIZE] {
    let mut out = zero_array();
    for i in 0..SIZE {
        out[i] = -l[i].clone() + 1u128;
    }
    out
}

/// XOR operation on a LinearCombination.
/// This requires one multiplication.
pub(crate) fn xor_bit<CS: ConstraintSystem>(cs: &mut CS, l: &LC, r: &LC) -> LC {
    let (l, r, m) = cs.multiply(l.clone(), r.clone());
    l + r - m * 2u128
}

/// XOR operation on an array of LinearCombination.
/// This requires `n` multiplications when the size of the array is `n`.
pub(crate) fn xor<CS: ConstraintSystem, const SIZE: usize>(
    cs: &mut CS,
    l: &[LC; SIZE],
    r: &[LC; SIZE],
) -> [LC; SIZE] {
    let mut out = zero_array();
    for i in 0..SIZE {
        let (l, r, m) = cs.multiply(l[i].clone(), r[i].clone());
        out[i] = l + r - m * 2u128;
    }
    out
}

/// AND operation on a LinearCombination.
/// This requires one multiplication.
pub(crate) fn and_bit<CS: ConstraintSystem>(cs: &mut CS, l: &LC, r: &LC) -> LC {
    let (_, _, m) = cs.multiply(l.clone(), r.clone());
    m.into()
}

/// AND operation on an array of LinearCombination.
/// This requires `n` multiplications when the size of the array is `n`.
pub(crate) fn and<CS: ConstraintSystem, const SIZE: usize>(
    cs: &mut CS,
    l: &[LC; SIZE],
    r: &[LC; SIZE],
) -> [LC; SIZE] {
    let mut out = zero_array();
    for i in 0..SIZE {
        let (_, _, m) = cs.multiply(l[i].clone(), r[i].clone());
        out[i] = m.into();
    }
    out
}

type WordU32 = Word<32>;
type WordU8 = Word<8>;

#[derive(Clone)]
pub struct Sha256State {
    state: [WordU32; 8],
    buffer: [WordU8; 64],
    state_w: [u32; 8],
    buffer_w: [u8; 64],

    len: u64,
    offset: u64,
    with_witness: bool,
}

impl Default for Sha256State {
    fn default() -> Self {
        let mut state: [WordU32; 8] = WordU32::zero_array();
        let buffer: [WordU8; 64] = WordU8::zero_array();

        for (w, h) in state.iter_mut().zip(H.iter()) {
            *w = WordU32::Full(Scalar::from(*h).into());
        }

        Self {
            state,
            buffer,
            state_w: H.clone(),
            buffer_w: [0; 64],
            len: 0,
            offset: 0,
            with_witness: false,
        }
    }
}

impl Sha256State {
    const WORD_SIZE: usize = 4; // Because u32
    const BLOCK_SIZE: usize = 16 * Self::WORD_SIZE;
    const DIGEST_SIZE: usize = 8 * Self::WORD_SIZE;
    const LENGTH_SIZE: usize = 8; // Because u64
    const LENGTH_OFFSET: usize = Self::BLOCK_SIZE - Self::LENGTH_SIZE;

    pub fn update<CS: ConstraintSystem, Bit: Into<LC> + Clone, Input: AsRef<[Bit]>>(
        mut self,
        cs: &mut CS,
        input: Input,
        input_w: Option<&[u8]>,
    ) -> Self {
        let input = input.as_ref();
        self.with_witness = input_w.is_some();

        let input_w = if self.with_witness {
            input_w.unwrap()
        } else {
            &[]
        };

        let needed = 64 - self.offset;
        if needed > input.len() as u64 {
            if self.with_witness {
                for (i, b) in input_w.iter().enumerate() {
                    self.buffer_w[i] = *b;
                }
            }

            for (i, b) in input.iter().enumerate() {
                self.buffer[i] = WordU8::Full(b.clone().into());
            }

            self.offset += input.len() as u64;
        } else {
            if self.with_witness {
                for (i, b) in input_w
                    [self.len as usize / 8..self.len as usize / 8 + needed as usize]
                    .iter()
                    .enumerate()
                {
                    self.buffer_w[self.offset as usize + i] = *b;
                }
            }

            for (i, b) in input[self.len as usize / 8..self.len as usize / 8 + needed as usize]
                .iter()
                .enumerate()
            {
                self.buffer[self.offset as usize + i] = WordU8::Full(b.clone().into());
            }

            if self.with_witness {
                Self::compress(
                    cs,
                    &mut self.state,
                    &mut self.buffer,
                    &mut Some(&mut self.state_w),
                    &mut Some(&mut self.buffer_w),
                    0,
                );
            } else {
                Self::compress(
                    cs,
                    &mut self.state,
                    &mut self.buffer,
                    &mut None,
                    &mut None,
                    0,
                );
            }

            let mut i = needed as usize;

            loop {
                let remain = input.len() - i;

                if remain < 64 {
                    if self.with_witness {
                        for (j, b) in input_w[i..i + remain].iter().enumerate() {
                            self.buffer_w[j] = *b;
                        }
                    }

                    for (j, b) in input[i..i + remain].iter().enumerate() {
                        self.buffer[j] = WordU8::Full(b.clone().into());
                    }

                    self.offset = remain as u64;
                    break;
                } else {
                    if self.with_witness {
                        Self::compress(
                            cs,
                            &mut self.state,
                            &mut self.buffer,
                            &mut Some(&mut self.state_w),
                            &mut Some(&mut self.buffer_w),
                            i,
                        );
                    } else {
                        Self::compress(
                            cs,
                            &mut self.state,
                            &mut self.buffer,
                            &mut None,
                            &mut None,
                            i,
                        );
                    }

                    i += 64;
                }
            }
        }

        self.len = self.len + input.len() as u64 * 8;

        self
    }

    pub fn finalize<CS: ConstraintSystem>(mut self, cs: &mut CS) -> [LC; 32] {
        //) -> [u8; 32] {
        let mut offset = self.offset;

        if self.with_witness {
            self.buffer_w[offset as usize] = 0x80;
        }

        self.buffer[offset as usize] = WordU8::Full(Scalar::from(0x80_u128).into());

        offset += 1;

        if offset > 64 - 8 {
            if self.with_witness {
                for b in self.buffer_w[offset as usize..64 - 8].iter_mut() {
                    *b = 0;
                }
            }

            for b in self.buffer[offset as usize..64 - 8].iter_mut() {
                *b = WordU8::Full(Scalar::from(0u128).into());
            }

            if self.with_witness {
                Self::compress(
                    cs,
                    &mut self.state,
                    &mut self.buffer,
                    &mut Some(&mut self.state_w),
                    &mut Some(&mut self.buffer_w),
                    0,
                );
            } else {
                Self::compress(
                    cs,
                    &mut self.state,
                    &mut self.buffer,
                    &mut None,
                    &mut None,
                    0,
                );
            }

            offset = 0;
        }

        if self.with_witness {
            for b in self.buffer_w[offset as usize..64 - 8].iter_mut() {
                *b = 0;
            }
        }

        for b in self.buffer[offset as usize..64 - 8].iter_mut() {
            *b = WordU8::Full(Scalar::from(0u128).into());
        }

        // Adding the lenght in bits at the end of the buffer
        if self.with_witness {
            for (i, b) in self.len.to_be_bytes().iter().enumerate() {
                self.buffer_w[Self::LENGTH_OFFSET + i] = *b;
            }
        }

        for (i, b) in self.len.to_be_bytes().iter().enumerate() {
            self.buffer[Self::LENGTH_OFFSET + i] = WordU8::Full(Scalar::from(*b).into());
        }
        if self.with_witness {
            Self::compress(
                cs,
                &mut self.state,
                &mut self.buffer,
                &mut Some(&mut self.state_w),
                &mut Some(&mut self.buffer_w),
                0,
            );
        } else {
            Self::compress(
                cs,
                &mut self.state,
                &mut self.buffer,
                &mut None,
                &mut None,
                0,
            );
        }

        let mut digest = zero_array();

        for (idx, (v, v_w)) in self
            .state
            .iter_mut()
            .zip(self.state_w.iter_mut())
            .enumerate()
        {
            let bits = v.as_bits(cs, Some(*v_w)).unwrap();

            let mut words: [LC; 4] = zero_array();

            for i in 0..bits.len() {
                words[3 - i / 8] = words[3 - i / 8].clone()
                    + Scalar::from(u128::pow(2, (i % 8) as u32)) * bits[i].clone();
            }

            for (i, word) in words.iter().enumerate() {
                digest[idx * 4 + i] = word.clone();
            }
        }

        digest
    }

    fn compress<CS: ConstraintSystem>(
        cs: &mut CS,
        state: &mut [WordU32; 8],
        buffer: &mut [WordU8; 64],
        state_w: &mut Option<&mut [u32; 8]>,
        buffer_w: &mut Option<&mut [u8; 64]>,
        offset: usize,
    ) {
        let mut w_w = [Some(0u32); 64];
        if let Some(b_w) = buffer_w {
            // Copy into to scheduler
            for (i, b) in b_w[..].chunks_exact(4).enumerate() {
                let mut v = [0u8; 4];
                v.copy_from_slice(b);
                let v: u32 = u32::from_be_bytes(v);

                w_w[i] = Some(v);
            }
        }

        let mut w: [WordU32; 64] = WordU32::zero_array();

        if let Some(b_w) = buffer_w {
            for (i, (bytes, bytes_w)) in buffer[..]
                .chunks_exact(4)
                .zip(b_w.chunks_exact(4))
                .enumerate()
            {
                let mut v = b[3].clone().as_word();
                v = v + Scalar::from(u128::pow(u128::pow(2, 8), 1)) * b[2].clone().as_word();
                v = v + Scalar::from(u128::pow(u128::pow(2, 8), 2)) * b[1].clone().as_word();
                v = v + Scalar::from(u128::pow(u128::pow(2, 8), 3)) * b[0].clone().as_word();

                w[i] = WordU32::Full(v);
                //let mut v: [LC; 32] = zero_array();

                //for j in 0..32 {
                    //v[j] = bytes[3 - j / 8]
                        //.clone()
                        //.as_bits(cs, Some(bytes_w[3 - j / 8].into()))
                        //.unwrap()[j % 8]
                        //.clone()
                        //* Scalar::from(u128::pow(2, j as u32));
                //}

                //w[i] = WordU32::Bits(v);
            }
        }

        for i in 16..64 {
            // (w[i-15] >>> 7) ^ (w[i-15] >>> 18) ^ (w[i-15] >> 3)
            let (mut s0, s0_w) = Self::sigma(cs, &mut w[i - 15], w_w[i - 15], (7, 18, 3));
            // (w[i-2] >>> 17) ^ (w[i-2] >>> 19) ^ (w[i-2] >> 10)
            let (mut s1, s1_w) = Self::sigma(cs, &mut w[i - 2], w_w[i - 2], (17, 19, 10));

            // w[i] = w[i-16] + s0 + w[i-7] + s1
            let (mut tmp1, tmp1_w) = wrapping_add(cs, &mut w[i - 16], &mut s0, w_w[i - 16], s0_w);
            let (mut tmp2, tmp2_w) = wrapping_add(cs, &mut w[i - 7], &mut s1, w_w[i - 7], s1_w);
            (w[i], w_w[i]) = wrapping_add(cs, &mut tmp1, &mut tmp2, tmp1_w, tmp2_w);
        }

        // Working variables
        let mut a = state[0].clone();
        let mut b = state[1].clone();
        let mut c = state[2].clone();
        let mut d = state[3].clone();
        let mut e = state[4].clone();
        let mut f = state[5].clone();
        let mut g = state[6].clone();
        let mut h = state[7].clone();

        // Working variables witness
        let (mut a_w, mut b_w, mut c_w, mut d_w, mut e_w, mut f_w, mut g_w, mut h_w) =
            if let Some(s_w) = state_w {
                (
                    Some(s_w[0]),
                    Some(s_w[1]),
                    Some(s_w[2]),
                    Some(s_w[3]),
                    Some(s_w[4]),
                    Some(s_w[5]),
                    Some(s_w[6]),
                    Some(s_w[7]),
                )
            } else {
                (None, None, None, None, None, None, None, None)
            };

        let mut state_w_copy = if let Some(s_w) = state_w {
            [
                Some(s_w[0]),
                Some(s_w[1]),
                Some(s_w[2]),
                Some(s_w[3]),
                Some(s_w[4]),
                Some(s_w[5]),
                Some(s_w[6]),
                Some(s_w[7]),
            ]
        } else {
            [None; 8]
        };

        for i in 0..64 {
            let mut k = WordU32::Full(Scalar::from(K[i]).into());

            // (e >>> 6) ^ (e >>> 11) ^ (e >>> 25)
            let (mut s1, s1_w) = Self::big_sigma(cs, &mut e, e_w, (6, 11, 25));

            // (e & f) ^ ((!e) & g)
            let (mut ch, ch_w) = Self::ch(cs, &mut e, &mut f, &mut g, e_w, f_w, g_w);

            // h + s1 + ch + k[i] + w[i]
            let (mut tmp1, tmp1_w) = wrapping_add(cs, &mut h, &mut s1, h_w, s1_w);
            let (mut tmp1, tmp1_w) = wrapping_add(cs, &mut tmp1, &mut ch, tmp1_w, ch_w);
            let (mut tmp1, tmp1_w) = if tmp1_w.is_some() {
                wrapping_add(cs, &mut tmp1, &mut k, tmp1_w, Some(K[i]))
            } else {
                wrapping_add(cs, &mut tmp1, &mut k, tmp1_w, None)
            };
            let (mut tmp1, tmp1_w) = if tmp1_w.is_some() {
                wrapping_add(cs, &mut tmp1, &mut w[i], tmp1_w, w_w[i])
            } else {
                wrapping_add(cs, &mut tmp1, &mut w[i], tmp1_w, None)
            };

            // (a >>> 2) ^ (a >>> 13) ^ (a >>> 22)
            let (mut s0, s0_w) = Self::big_sigma(cs, &mut a, a_w, (2, 13, 22));
            // (a & b) ^ (a & c) ^ (b & c)
            let (mut maj, maj_w) = Self::maj(cs, &mut a, &mut b, &mut c, a_w, b_w, c_w);

            // s0 + maj
            let (mut tmp2, tmp2_w) = wrapping_add(cs, &mut s0, &mut maj, s0_w, maj_w);

            (h, h_w) = (g, g_w);
            (g, g_w) = (f, f_w);
            (f, f_w) = (e, e_w);
            // d + tmp1
            (e, e_w) = wrapping_add(cs, &mut d, &mut tmp1, d_w, tmp1_w);
            (d, d_w) = (c, c_w);
            (c, c_w) = (b, b_w);
            (b, b_w) = (a, a_w);
            // tmp1 + tmp2
            (a, a_w) = wrapping_add(cs, &mut tmp1, &mut tmp2, tmp1_w, tmp2_w);
        }

        // h0 = h0 + a
        // ...
        (state[0], state_w_copy[0]) = wrapping_add(cs, &mut state[0], &mut a, state_w_copy[0], a_w);
        (state[1], state_w_copy[1]) = wrapping_add(cs, &mut state[1], &mut b, state_w_copy[1], b_w);
        (state[2], state_w_copy[2]) = wrapping_add(cs, &mut state[2], &mut c, state_w_copy[2], c_w);
        (state[3], state_w_copy[3]) = wrapping_add(cs, &mut state[3], &mut d, state_w_copy[3], d_w);
        (state[4], state_w_copy[4]) = wrapping_add(cs, &mut state[4], &mut e, state_w_copy[4], e_w);
        (state[5], state_w_copy[5]) = wrapping_add(cs, &mut state[5], &mut f, state_w_copy[5], f_w);
        (state[6], state_w_copy[6]) = wrapping_add(cs, &mut state[6], &mut g, state_w_copy[6], g_w);
        (state[7], state_w_copy[7]) = wrapping_add(cs, &mut state[7], &mut h, state_w_copy[7], h_w);

        if let Some(s_w) = state_w {
            for (b_c, b) in state_w_copy.iter().zip(s_w.iter_mut()) {
                *b = b_c.unwrap();
            }
        }
    }

    fn sigma<CS: ConstraintSystem>(
        cs: &mut CS,
        e: &mut WordU32,
        e_witness: Option<u32>,
        rot: (usize, usize, usize),
    ) -> (WordU32, Option<u32>) {
        fn with_witness(e: Option<u32>, rot: (usize, usize, usize)) -> Option<u32> {
            match e {
                Some(e) => Some(
                    e.rotate_right(rot.0 as u32)
                        ^ e.rotate_right(rot.1 as u32)
                        ^ (e >> rot.2 as u32),
                ),
                _ => None,
            }
        }

        let e_bits = e.as_bits(cs, e_witness).unwrap();

        let rot0 = rotate_right(e_bits, rot.0);
        let rot1 = rotate_right(e_bits, rot.1);
        let rot2 = shift_right(e_bits, rot.2);

        let rot01 = xor(cs, &rot0, &rot1);
        let result = xor(cs, &rot01, &rot2);

        (Word::Bits(result), with_witness(e_witness, rot))
    }

    fn big_sigma<CS: ConstraintSystem>(
        cs: &mut CS,
        e: &mut WordU32,
        e_witness: Option<u32>,
        rot: (usize, usize, usize),
    ) -> (WordU32, Option<u32>) {
        fn with_witness(e: Option<u32>, rot: (usize, usize, usize)) -> Option<u32> {
            match e {
                Some(e) => Some(
                    e.rotate_right(rot.0 as u32)
                        ^ e.rotate_right(rot.1 as u32)
                        ^ e.rotate_right(rot.2 as u32),
                ),
                _ => None,
            }
        }

        let e_bits = e.as_bits(cs, e_witness).unwrap();

        let rot0 = rotate_right(e_bits, rot.0);
        let rot1 = rotate_right(e_bits, rot.1);
        let rot2 = rotate_right(e_bits, rot.2);

        let rot01 = xor(cs, &rot0, &rot1);
        let result = xor(cs, &rot01, &rot2);

        (Word::Bits(result), with_witness(e_witness, rot))
    }

    fn ch<CS: ConstraintSystem>(
        cs: &mut CS,
        e: &mut WordU32,
        f: &mut WordU32,
        g: &mut WordU32,
        e_witness: Option<u32>,
        f_witness: Option<u32>,
        g_witness: Option<u32>,
    ) -> (WordU32, Option<u32>) {
        fn with_witness(e: Option<u32>, f: Option<u32>, g: Option<u32>) -> Option<u32> {
            match (e, f, g) {
                (Some(e), Some(f), Some(g)) => Some((e & f) ^ ((!e) & g)),
                _ => None,
            }
        }

        let bits_e = e.as_bits(cs, e_witness).unwrap();
        let bits_f = f.as_bits(cs, f_witness).unwrap();
        let bits_g = g.as_bits(cs, g_witness).unwrap();

        // (e & f) ^ (!e & g)
        let ef = and(cs, bits_e, bits_f);
        let not_e = not(bits_e);
        let note_g = and(cs, &not_e, bits_g);

        let result = xor(cs, &ef, &note_g);

        (
            Word::Bits(result),
            with_witness(e_witness, f_witness, g_witness),
        )
    }

    fn maj<CS: ConstraintSystem>(
        cs: &mut CS,
        a: &mut WordU32,
        b: &mut WordU32,
        c: &mut WordU32,
        a_witness: Option<u32>,
        b_witness: Option<u32>,
        c_witness: Option<u32>,
    ) -> (WordU32, Option<u32>) {
        fn with_witness(a: Option<u32>, b: Option<u32>, c: Option<u32>) -> Option<u32> {
            match (a, b, c) {
                (Some(a), Some(b), Some(c)) => Some((a & b) ^ (a & c) ^ (b & c)),
                _ => None,
            }
        }

        let bits_a = a.as_bits(cs, a_witness).unwrap();
        let bits_b = b.as_bits(cs, b_witness).unwrap();
        let bits_c = c.as_bits(cs, c_witness).unwrap();

        let ab = and(cs, bits_a, bits_b);
        let ac = and(cs, bits_a, bits_c);
        let bc = and(cs, bits_b, bits_c);

        let tmp = xor(cs, &ab, &ac);
        let result = xor(cs, &tmp, &bc);

        (
            Word::Bits(result),
            with_witness(a_witness, b_witness, c_witness),
        )
    }
}

#[derive(Clone, Debug)]
enum Word<const SIZE: usize> {
    Bits([LC; SIZE]),
    Full(LC),
    Both(LC, [LC; SIZE]),
}

impl<const SIZE: usize> Word<SIZE> {
    fn zero_array<const N: usize>() -> [Self; N] {
        let mut a: [MaybeUninit<Self>; N] = unsafe { MaybeUninit::uninit().assume_init() };

        for k in a.iter_mut() {
            *k = MaybeUninit::new(Default::default());
        }

        unsafe { mem::transmute_copy(&a) }
    }

    fn as_word(&mut self) -> LC {
        match self {
            Self::Full(val) | Self::Both(val, _) => val.clone(),
            Self::Bits(bits) => {
                let sum = bits
                    .iter()
                    .enumerate()
                    .map(|(i, bit): (usize, &LC)| bit.clone() * Scalar::from(1u128 << i))
                    .fold(LC::default(), |a, b| a + b);
                *self = Self::Both(sum.clone(), (*bits).clone());
                sum
            }
        }
    }

    fn unwrap_as_bits(&self) -> &[LC; SIZE] {
        match self {
            Self::Bits(bits) => bits,
            Self::Both(_, bits) => bits,
            Self::Full(_num) => panic!("unwrap_as_bits"),
        }
    }

    fn as_bits<CS: ConstraintSystem + ?Sized>(
        &mut self,
        cs: &mut CS,
        witness: Option<u32>,
    ) -> Result<&[LC; SIZE], R1CSError> {
        match self {
            Self::Full(num) => {
                let (sum, bits) = u32_into_bits::<_, SIZE>(cs, witness)?; // XXX(thvdveld): this can fail for other types than u32
                cs.constrain(num.clone() - sum.clone());
                *self = Self::Both(sum, bits);
                Ok(self.unwrap_as_bits())
            }
            Self::Bits(bits) => Ok(bits),
            Self::Both(_, bits) => Ok(bits),
        }
    }

    fn into_word(self) -> LC {
        match self {
            Self::Full(val) | Self::Both(val, _) => val,
            Self::Bits(bits) => bits
                .iter()
                .enumerate()
                .map(|(i, bit): (usize, &LC)| bit.clone() * Scalar::from(1u32 << i))
                .fold(LC::default(), |a, b| a + b),
        }
    }
}

fn wrapping_add<CS: ConstraintSystem, const SIZE: usize>(
    cs: &mut CS,
    l: &mut Word<SIZE>,
    r: &mut Word<SIZE>,
    l_witness: Option<u32>, // XXX: only works for u32
    r_witness: Option<u32>, // XXX: only works for u32
) -> (Word<SIZE>, Option<u32>) {
    let (overflow_bit, result_witness) = match (l_witness, r_witness) {
        (Some(witness), Some(other_witness)) => {
            let (o, f) = witness.overflowing_add(other_witness);
            (Some(f), Some(o))
        }
        (Some(_), None) | (None, Some(_)) => {
            unreachable!("Partial witness not supported")
        }
        (None, None) => (None, None),
    };

    let overflow_bit = allocate_bit(cs, overflow_bit).unwrap() * (1u128 << 32);

    let result = l.clone() + r.clone() - overflow_bit.into();
    (result, result_witness)
}

impl<const SIZE: usize> core::ops::SubAssign<Word<SIZE>> for Word<SIZE> {
    fn sub_assign(&mut self, mut other: Self) {
        *self = Word::Full(self.as_word() - other.as_word());
    }
}

impl<const SIZE: usize> core::ops::Add<Word<SIZE>> for Word<SIZE> {
    type Output = Word<SIZE>;

    fn add(self, other: Self) -> Self::Output {
        (self.into_word() + other.into_word()).into()
    }
}

impl<const SIZE: usize> core::ops::Sub<Word<SIZE>> for Word<SIZE> {
    type Output = Word<SIZE>;

    fn sub(self, other: Self) -> Self::Output {
        (self.into_word() - other.into_word()).into()
    }
}

impl<const SIZE: usize> From<u32> for Word<SIZE> {
    fn from(s: u32) -> Self {
        let sum = s.into();
        let mut bits = zero_array();
        for (i, bit) in bits.iter_mut().enumerate() {
            if s & (1 << i) != 0 {
                *bit = Scalar::one().into();
            }
        }

        Self::Both(sum, bits)
    }
}

impl<const SIZE: usize> From<LC> for Word<SIZE> {
    fn from(lc: LC) -> Self {
        Self::Full(lc)
    }
}

impl<const SIZE: usize> Default for Word<SIZE> {
    fn default() -> Self {
        Self::Both(Default::default(), zero_array())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use bulletproofs::r1cs::*;
    use bulletproofs::{BulletproofGens, PedersenGens};
    use curve25519_dalek::scalar::Scalar;
    use failure::*;
    use rayon::prelude::*;

    #[test]
    fn sha256_hash() {
        let cases: [(&[u8], [u8; 32]); 1] = [
            (
                b"",
                [
                    0xe3, 0xb0, 0xc4, 0x42, 0x98, 0xfc, 0x1c, 0x14, 0x9a, 0xfb, 0xf4, 0xc8, 0x99,
                    0x6f, 0xb9, 0x24, 0x27, 0xae, 0x41, 0xe4, 0x64, 0x9b, 0x93, 0x4c, 0xa4, 0x95,
                    0x99, 0x1b, 0x78, 0x52, 0xb8, 0x55,
                ],
            ),
            //(
            //b"Hello Rust",
            //[
            //0xdc, 0x5d, 0x63, 0x13, 0x4f, 0xb6, 0x96, 0x62, 0x6c, 0x4b, 0xf2, 0x8e, 0x12,
            //0x32, 0x43, 0x4a, 0xb0, 0x40, 0xac, 0xc1, 0xa, 0x66, 0xcf, 0xee, 0x55, 0xda,
            //0xcd, 0xd7, 0xd, 0xae, 0x82, 0xa3,
            //],
            //),
            //(
            //b"Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the future, sunscreen would be it.",
            //[
            //0x34, 0xdb, 0xfc, 0xbb, 0xe7, 0x3c, 0x59, 0x19, 0x5a, 0x7a, 0xc5, 0x63, 0xb4,
            //0x1b, 0x82, 0xf3, 0x34, 0x84, 0x50, 0x53, 0xc7, 0x7, 0xb8, 0x3d, 0x81, 0x79,
            //0xd7, 0xb1, 0x65, 0x77, 0x8b, 0x19,
            //],
            //),
        ];

        for (input, expected) in &cases {
            let pc_gens = PedersenGens::default();
            let bp_gens = BulletproofGens::new(2048 * 32 * 10, 1);

            let now = std::time::Instant::now();
            let proof = {
                let mut transcript = merlin::Transcript::new(b"sha256-compression");
                let mut prover = Prover::new(&pc_gens, &mut transcript);

                let mut state = Sha256State::default();

                let result = state
                    .update(&mut prover, input, Some(input))
                    .finalize(&mut prover);

                //println!("{:x?}", result);
                for (b, b_expected) in result.iter().zip(expected.iter()) {
                    prover.constrain(b.clone() - LC::from(*b_expected));
                }

                prover.prove(&bp_gens).unwrap()
            };

            println!(
                "Proof generation took {}us for {} bytes",
                now.elapsed().as_micros(),
                input.len()
            );

            {
                let now = std::time::Instant::now();
                let mut transcript = merlin::Transcript::new(b"sha256-compression");
                let mut verifier = Verifier::new(&mut transcript);

                let mut state = Sha256State::default();

                let result = state
                    .update(&mut verifier, input, None)
                    .finalize(&mut verifier);

                for (b, b_expected) in result.iter().zip(expected.iter()) {
                    verifier.constrain(b.clone() - LC::from(*b_expected));
                }

                dbg!(verifier.metrics());
                assert_eq!(verifier.verify(&proof, &pc_gens, &bp_gens), Ok(()));
                println!(
                    "Verify took {}us for {} bytes",
                    now.elapsed().as_micros(),
                    input.len()
                );
            }
        }
        //assert!(false);
    }
}
