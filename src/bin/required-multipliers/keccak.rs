use smoking_gun::keccak::Sha3_256;

use bulletproofs::r1cs::{ConstraintSystem, Prover};
use bulletproofs::PedersenGens;

pub const MAX_LEN: usize = 256;

pub fn compute_sha3_256(len: usize) -> bulletproofs::r1cs::Metrics {
    let mut transcript = merlin::Transcript::new(b"keccak-f");
    let pc_gens = PedersenGens::default();

    let input = vec![0u8; len];

    let mut prover = Prover::new(&pc_gens, &mut transcript);

    Sha3_256::gadget_from_bytes(&mut prover, input.len(), &input as &[u8]).unwrap();
    prover.metrics()
}
