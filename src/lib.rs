#![feature(destructuring_assignment)]

#[cfg(feature = "bristol")]
pub mod bristol;
#[cfg(feature = "keccak")]
pub mod keccak;

#[cfg(feature = "sha2")]
pub mod sha2;

pub mod aead;

#[cfg(feature = "keccak")]
pub mod hash_then_prove;

pub mod utils;
