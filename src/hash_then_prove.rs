use bulletproofs::r1cs::{ConstraintSystem, LinearCombination};
use bulletproofs::PedersenGens;
use curve25519_dalek::ristretto::RistrettoPoint;
use curve25519_dalek::scalar::Scalar;

use tiny_keccak::Keccak;

pub fn commit_multi<Witness: AsRef<[u8]>>(
    pc_gens: &PedersenGens,
    witnesses: impl IntoIterator<Item = Witness>,
    blinding: Scalar,
) -> RistrettoPoint {
    // Simple tuplehash-like 256-bit secure variant
    let mut k = Keccak::new(136, 0x2B);

    for witness in witnesses.into_iter() {
        let witness = witness.as_ref();
        let len = (witness.len() as u64).to_le_bytes();
        k.update(&len);
        k.update(witness);
    }

    let mut x = k.xof();
    let mut hash = [0u8; 32];
    x.squeeze(&mut hash);

    pc_gens.commit(Scalar::from_bytes_mod_order(hash), blinding)
}

pub trait HashThenProve {
    fn hashed_vector_commitment<'a>(
        &self,
        witness: impl IntoIterator<Item = &'a [u8]>,
        blinding: Scalar,
    ) -> LinearCombination;
}

impl<T: ConstraintSystem> HashThenProve for T {
    fn hashed_vector_commitment<'a>(
        &self,
        _witness: impl IntoIterator<Item = &'a [u8]>,
        _blinding: Scalar,
    ) -> LinearCombination {
        unimplemented!()
    }
}
