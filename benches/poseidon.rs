use criterion::criterion_group;
use criterion::criterion_main;
use criterion::Criterion;

fn poseidon_preimage(_c: &mut Criterion) {}

criterion_group! {
    name = poseidon;
    config = Criterion::default();
    targets = poseidon_preimage
}
criterion_main!(poseidon);
