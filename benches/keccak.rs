#![feature(const_generics)]

use smoking_gun::keccak;

use criterion::criterion_group;
use criterion::criterion_main;
use criterion::Criterion;

use bulletproofs::r1cs::*;
use bulletproofs::r1cs::{Prover, Verifier};
use bulletproofs::{BulletproofGens, PedersenGens};
use curve25519_dalek::ristretto::RistrettoPoint;
use curve25519_dalek::scalar::Scalar;
use merlin::Transcript;

use keccak::Sha3_256;

// Make a gadget that adds constraints to a ConstraintSystem, such that the
// y variables are constrained to be a valid shuffle of the x variables.
struct KShuffleGadget {}

impl KShuffleGadget {
    fn fill_cs<CS: RandomizableConstraintSystem>(
        cs: &mut CS,
        x: Vec<Variable>,
        y: Vec<Variable>,
    ) -> Result<(), R1CSError> {
        let one = Scalar::one();

        assert_eq!(x.len(), y.len());

        let k = x.len();
        if k == 1 {
            cs.constrain([(x[0], -one), (y[0], one)].iter().collect());
            return Ok(());
        }

        cs.specify_randomized_constraints(move |cs| {
            let z = cs.challenge_scalar(b"shuffle challenge");

            // Make last x multiplier for i = k-1 and k-2
            let (_, _, last_mulx_out) = cs.multiply(x[k - 1] - z, x[k - 2] - z);

            // Make multipliers for x from i == [0, k-3]
            let first_mulx_out = (0..k - 2).rev().fold(last_mulx_out, |prev_out, i| {
                let (_, _, o) = cs.multiply(prev_out.into(), x[i] - z);
                o
            });

            // Make last y multiplier for i = k-1 and k-2
            let (_, _, last_muly_out) = cs.multiply(y[k - 1] - z, y[k - 2] - z);

            // Make multipliers for y from i == [0, k-3]
            let first_muly_out = (0..k - 2).rev().fold(last_muly_out, |prev_out, i| {
                let (_, _, o) = cs.multiply(prev_out.into(), y[i] - z);
                o
            });

            // Constrain last x mul output and last y mul output to be equal
            cs.constrain(
                [(first_muly_out, -one), (first_mulx_out, one)]
                    .iter()
                    .collect(),
            );

            Ok(())
        })
    }
}

fn k_shuffle(c: &mut Criterion) {
    const MAX: usize = 8;

    c.bench_function_over_inputs(
        &format!("k-shuffle"),
        |b, input_len| {
            let pc_gens = PedersenGens::default();
            let bp_gens = BulletproofGens::new(2 * (*input_len), 1);

            let input: Vec<_> = (0..*input_len)
                .map(|_| Scalar::random(&mut rand::thread_rng()))
                .collect();

            use rand::seq::SliceRandom;

            let mut permutation = input.clone();
            permutation.shuffle(&mut rand::thread_rng());

            let (input_commitments, input_blindings): (Vec<_>, Vec<_>) = input
                .iter()
                .map(|v| {
                    let blinding = Scalar::random(&mut rand::thread_rng());
                    (pc_gens.commit(*v, blinding), blinding)
                })
                .unzip();

            let (permutation_commitments, permutation_blindings): (Vec<_>, Vec<_>) = permutation
                .iter()
                .map(|v| {
                    let blinding = Scalar::random(&mut rand::thread_rng());
                    (pc_gens.commit(*v, blinding), blinding)
                })
                .unzip();

            b.iter(move || {
                let mut transcript = Transcript::new(b"k-shuffle-vec");
                let mut prover = Prover::new(&pc_gens, &mut transcript);

                let (_, variables_left): (Vec<_>, _) = input
                    .iter()
                    .zip(input_commitments.iter().zip(input_blindings.iter()))
                    .map(
                        |(value, (_commitment, blinding)): (
                            &Scalar,
                            (&RistrettoPoint, &Scalar),
                        )| {
                            prover.commit(value.clone(), blinding.clone())
                        },
                    )
                    .unzip();
                let (_, variables_right): (Vec<_>, _) = permutation
                    .iter()
                    .zip(
                        permutation_commitments
                            .iter()
                            .zip(permutation_blindings.iter()),
                    )
                    .map(
                        |(value, (_commitment, blinding)): (
                            &Scalar,
                            (&RistrettoPoint, &Scalar),
                        )| {
                            prover.commit(value.clone(), blinding.clone())
                        },
                    )
                    .unzip();

                KShuffleGadget::fill_cs(&mut prover, variables_left, variables_right).unwrap();

                prover.prove(&bp_gens)
            });
        },
        (0..MAX).map(|i| 1 << i),
    );

    #[cfg(feature = "pvc")]
    c.bench_function_over_inputs(
        &format!("k-shuffle-vec"),
        |b, input_len| {
            let pc_gens = PedersenGens::default();
            let bp_gens = BulletproofGens::new(2 * (*input_len), 1);

            let input: Vec<_> = (0..*input_len)
                .map(|_| Scalar::random(&mut rand::thread_rng()))
                .collect();

            use rand::seq::SliceRandom;

            let mut permutation = input.clone();
            permutation.shuffle(&mut rand::thread_rng());

            let blinding_left = Scalar::random(&mut rand::thread_rng());
            let commitment_left = pc_gens.commit_multi(&bp_gens, &input, &blinding_left);

            let blinding_right = Scalar::random(&mut rand::thread_rng());
            let commitment_right = pc_gens.commit_multi(&bp_gens, &permutation, &blinding_right);

            b.iter(move || {
                let mut transcript = Transcript::new(b"k-shuffle-vec");
                let mut prover = Prover::new(&pc_gens, &mut transcript);

                let variables_left = prover.recommit_multi(
                    input.clone(),
                    blinding_left.clone(),
                    commitment_left.compress(),
                );
                let variables_right = prover.ecommit_multi(
                    permutation.clone(),
                    blinding_right.clone(),
                    commitment_right.compress(),
                );

                KShuffleGadget::fill_cs(&mut prover, variables_left, variables_right).unwrap();

                prover.prove(&bp_gens)
            });
        },
        (0..MAX).map(|i| 1 << i),
    );
}

fn sha3_256(c: &mut Criterion) {
    c.bench_function_over_inputs(
        &format!("SHA3-256"),
        move |b, input_len| {
            let pc_gens = PedersenGens::default();
            let bp_gens = BulletproofGens::new(2 * 64 * 100 * 24, 1);

            let mut transcript = merlin::Transcript::new(b"keccak-f");

            let input = vec![0u8; *input_len];

            b.iter(move || {
                let mut prover = Prover::new(&pc_gens, &mut transcript);
                Sha3_256::gadget_from_bytes(&mut prover, input.len(), &input as &[u8]).unwrap();
                prover.prove(&bp_gens)
            });
        },
        std::iter::once(0).chain((0..8).map(|i| 1 << i)),
    );
}

fn keccak_f_permute(c: &mut Criterion) {
    macro_rules! bench_keccak_f {
        ($lane:literal) => {
            c.bench_function(
                &format!("Keccak {} full permutation", 25 * $lane),
                move |b| {
                    let pc_gens = PedersenGens::default();
                    let bp_gens = BulletproofGens::new($lane * 100 * 24, 1);

                    let mut transcript = merlin::Transcript::new(b"keccak-f");
                    b.iter(move || {
                        let mut prover = Prover::new(&pc_gens, &mut transcript);
                        let mut state = keccak::KeccakState::<$lane>::default();
                        state.permute(&mut prover);
                        prover.prove(&bp_gens)
                    });
                },
            );
        };
    }

    // bench_keccak_f!(1);
    // bench_keccak_f!(2);
    // bench_keccak_f!(4);
    bench_keccak_f!(8);
    bench_keccak_f!(16);
    // bench_keccak_f!(32);
    bench_keccak_f!(64);
}

fn keccak_f_round(c: &mut Criterion) {
    macro_rules! bench_keccak_f {
        ($lane:literal) => {
            c.bench_function(&format!("Keccak {} 1 round", 25 * $lane), |b| {
                let pc_gens = PedersenGens::default();
                let bp_gens = BulletproofGens::new($lane * 100, 1);

                let mut transcript = merlin::Transcript::new(b"keccak-f");
                b.iter(move || {
                    let mut prover = Prover::new(&pc_gens, &mut transcript);
                    let mut state = keccak::KeccakState::<$lane>::default();
                    state.round(&mut prover, 0);
                    prover.prove(&bp_gens)
                });
            });
        };
    }

    // bench_keccak_f!(1);
    // bench_keccak_f!(2);
    // bench_keccak_f!(4);
    bench_keccak_f!(8);
    bench_keccak_f!(16);
    // bench_keccak_f!(32);
    bench_keccak_f!(64);
}

criterion_group! {
    name = keccak;
    config = Criterion::default().sample_size(10);
    targets =
        keccak_f_round,
        keccak_f_permute,
        sha3_256,
}

criterion_group! {
    name = shuffle;
    config = Criterion::default();
    targets =
        k_shuffle,
}

criterion_main!(keccak, shuffle);
